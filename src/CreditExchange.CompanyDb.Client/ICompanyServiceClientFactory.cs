﻿
using CreditExchange.CompanyDb.Abstractions;
using LendFoundry.Security.Tokens;

namespace CreditExchange.CompanyDb.Client
{
    /// <summary>
    /// Company Db Service Client Factory interface
    /// </summary>
    public interface ICompanyServiceClientFactory
    {
        /// <summary>
        /// Creates the specified reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns></returns>
        ICompanyService Create(ITokenReader reader);
    }
}
