﻿
using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;


namespace CreditExchange.CompanyDb.Client
{
    /// <summary>
    /// Company Db Service Client Extensions class
    /// </summary>
    public static class CompanyServiceClientExtensions
    {
        /// <summary>
        /// Adds the application service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="port">The port.</param>
        /// <returns></returns>
        public static IServiceCollection AddCompanyService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ICompanyServiceClientFactory>(p => new CompanyServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ICompanyServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
