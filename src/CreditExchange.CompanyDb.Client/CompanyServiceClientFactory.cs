﻿using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Foundation.Client;
using CreditExchange.CompanyDb.Abstractions;

namespace CreditExchange.CompanyDb.Client
{
    /// <summary>
    /// Company Db Service Client Factory class
    /// </summary>
    /// <seealso cref="ICompanyServiceClientFactory" />
    public class CompanyServiceClientFactory : ICompanyServiceClientFactory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyServiceClientFactory"/> class.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="port">The port.</param>
        public CompanyServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        /// <summary>
        /// Gets the provider.
        /// </summary>
        /// <value>
        /// The provider.
        /// </value>
        private IServiceProvider Provider { get; }

        /// <summary>
        /// Gets the endpoint.
        /// </summary>
        /// <value>
        /// The endpoint.
        /// </value>
        private string Endpoint { get; }

        /// <summary>
        /// Gets the port.
        /// </summary>
        /// <value>
        /// The port.
        /// </value>
        private int Port { get; }

        /// <summary>
        /// Creates the specified reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns></returns>
        public ICompanyService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new CompanyService(client);
        }

    }
}
