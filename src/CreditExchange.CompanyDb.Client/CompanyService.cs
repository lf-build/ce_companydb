﻿using CreditExchange.CompanyDb.Abstractions;
using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace CreditExchange.CompanyDb.Client
{
    /// <summary>
    /// Company Db Service class
    /// </summary>
    /// <seealso cref="Abstractions.ICompanyDbService" />
    public class CompanyService : ICompanyService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyService"/> class.
        /// </summary>
        /// <param name="client">The client.</param>
        public CompanyService(IServiceClient client)
        {
            Client = client;
        }

        /// <summary>
        /// Gets the client.
        /// </summary>
        /// <value>
        /// The client.
        /// </value>
        private IServiceClient Client { get; }

        public Task<RuleResult> GetCompanyCategory(string entityType, string entityId, ICompanyWithDirectors companyWithDirectors, string cin)
        {
            var request = new RestRequest("{entityType}/{entityId}/company/category/{cin}", Method.GET);
            request.AddUrlSegment("cin", cin);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            return Client.ExecuteAsync<RuleResult>(request);
        }

        /// <summary>
        /// Gets the company with directors asynchronous.
        /// </summary>
        /// <param name="cin">The cin.</param>
        /// <returns></returns>
        public Task<CompanyWithDirectors> GetCompanyWithDirectors(string entityType, string entityId, string cin)
        {
            var request = new RestRequest("{entityType}/{entityId}/company/directors/{cin}", Method.GET);
            request.AddUrlSegment("cin", cin);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            return Client.ExecuteAsync<CompanyWithDirectors>(request);
        }

        /// <summary>
        /// Searches the by company name or cin asynchronous.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns></returns>
        public Task<CompanySearchResponses> SearchByNameOrCin(string entityType, string entityId, string searchText)
        {
            var request = new RestRequest("{entityType}/{entityId}/company/search/{s}", Method.GET);
            request.AddUrlSegment("s", searchText);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            return Client.ExecuteAsync<CompanySearchResponses>(request);
        }
        public Task<CompanySearchResponses> SearchByName(string entityType, string entityId, string searchText)
        {
            var request = new RestRequest("{entityType}/{entityId}/company/search/byname/{s}", Method.GET);
            request.AddUrlSegment("s", searchText);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            return Client.ExecuteAsync<CompanySearchResponses>(request);
        }
    }
}
