﻿using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Threading.Tasks;
using CreditExchange.CompanyDb.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using LendFoundry.Clients.DecisionEngine;
using System.Linq;
using LendFoundry.Foundation.Services;

namespace CreditExchange.CompanyDb
{
    public sealed class UnlistedGovtRule : CompanyCategorizationRuleBase
    {
        private readonly string _entityId;
        private readonly string _entityType;
        private readonly CompanyCategorizationRulesParameters _categorizationRulesParameters;
        private readonly RuleResult _ruleResult;

        public UnlistedGovtRule(CompanyCategorizationRulesParameters categorizationRulesParameters, RuleResult ruleResult, string entityType, string entityId)
        {
            _categorizationRulesParameters = categorizationRulesParameters;
            this._ruleResult = ruleResult;
            this._entityType = entityType;
            this._entityId = entityId;

        }


        public override void ExecuteRule()
        {
            if (_categorizationRulesParameters.Navratna)
            {
                _ruleResult.Result = "A";
                _ruleResult.RuleName = nameof(UnlistedGovtRule);
                return;
            }
            SetNextRule(new UnlistedGovtBankCategoryRule(_categorizationRulesParameters, _ruleResult, _entityType, _entityId));
        }
    }

    public sealed class UnlistedGovtBankCategoryRule : CompanyCategorizationRuleBase
    {
        private readonly CompanyCategorizationRulesParameters _categorizationRulesParameters;
        private string _entityId;
        private string _entityType;
        private readonly RuleResult _ruleResult;

        public UnlistedGovtBankCategoryRule(CompanyCategorizationRulesParameters categorizationRulesParameters, RuleResult ruleResult, string _entityType, string _entityId)
        {
            _categorizationRulesParameters = categorizationRulesParameters;
            _ruleResult = ruleResult;
            this._entityType = _entityType;
            this._entityId = _entityId;
            ExecuteRule();
        }

        public override void ExecuteRule()
        {
            if (!string.IsNullOrWhiteSpace(_categorizationRulesParameters.BankCategory))
            {
                _ruleResult.Result = _categorizationRulesParameters.BankCategory;
                _ruleResult.RuleName = "UnlistedBankCategory";
                return;
            }
            SetNextRule(new UnlistedGovtScoreCardRule(
                _categorizationRulesParameters,
                _ruleResult, 
                _entityType,
                _entityId));
        }
    }

    public sealed class UnlistedGovtScoreCardRule : CompanyCategorizationRuleBase
    {

        private readonly CompanyCategorizationRulesParameters _categorizationRulesParameters;
        private readonly RuleResult _ruleResult;
        private string _entityType;
        private string _entityId;
        private IDecisionEngineService DecisionEngineService;


        public UnlistedGovtScoreCardRule(CompanyCategorizationRulesParameters categorizationRulesParameters, RuleResult ruleResult, string _entityType, string _entityId)
        {
            _categorizationRulesParameters = categorizationRulesParameters;
            _ruleResult = ruleResult;
            Configuration = ServiceLocator.ServiceProvider.GetService<Configuration>();
            // ExpressionService = ServiceLocator.ServiceProvider.GetService<IEvaluationService>();
            DecisionEngineService = ServiceLocator.ServiceProvider.GetService<IDecisionEngineService>();
            this._entityType = _entityType;
            this._entityId = _entityId;
            ExecuteRule();
        }

        private Configuration Configuration { get; }
        public string RuleName { get; } = "GovernmentUnlisted";

        public override void ExecuteRule()
        {

            var directorAffiliations = new List<dynamic>();
            foreach (var companyDirector in _categorizationRulesParameters.DirectorsList)
            {
                dynamic c = new ExpandoObject();
                c.Age = companyDirector.AgeOfCompany;
                c.Capital = companyDirector.PaidUpCapital.ToString(CultureInfo.InvariantCulture);
                directorAffiliations.Add(c);
            }

            if (Configuration == null)
                throw new NotFoundException("Configuration is not set.");
            List<CompanyDBDetail> groupConfiguration;
            if (!Configuration.TryGetValue("application", out groupConfiguration))
                throw new NotFoundException($"The {RuleName} not found");

            CompanyDBDetail extractedScoreCardRules = new CompanyDBDetail();
            if (!string.IsNullOrWhiteSpace(RuleName))
                extractedScoreCardRules = groupConfiguration.Where(r => r.Name == RuleName).FirstOrDefault();

            var payload = new { DirectorAffiliations = directorAffiliations, AgeOfCompany = _categorizationRulesParameters.AgeOfCompany, PaidUpCapital = _categorizationRulesParameters.PaidUpCapital, AuthCapital = _categorizationRulesParameters.AuthorizedCapital, Revenue = _categorizationRulesParameters.Revenue, Profit = _categorizationRulesParameters.Profit, IsState = _categorizationRulesParameters.IsState };
            var executionResult = DecisionEngineService.Execute<dynamic, CompanyCategoryResult>(extractedScoreCardRules.Rule.RuleName,new { payload = payload });
            var result = executionResult;
            if (result.Status == true)
            {
                _ruleResult.Result = result.Result.ToString();
                _ruleResult.RuleName = nameof(UnlistedGovtScoreCardRule);
            }

        }
    }

    public sealed class GovtUnlistedFinancialScoreCard : CompanyCategorizationRuleBase
    {
        private readonly CompanyCategorizationRulesParameters _categorizationRulesParameters;
        private string _entityId;
        private string _entityType;
        private readonly RuleResult _ruleResult;
        private IDecisionEngineService DecisionEngineService;
        public GovtUnlistedFinancialScoreCard(CompanyCategorizationRulesParameters categorizationRulesParameters, RuleResult ruleResult, string _entityType, string _entityId)
        {
            _categorizationRulesParameters = categorizationRulesParameters;
            _ruleResult = ruleResult;
            Configuration = ServiceLocator.ServiceProvider.GetService<Configuration>();
            DecisionEngineService = ServiceLocator.ServiceProvider.GetService<IDecisionEngineService>();
            this._entityType = _entityType;
            this._entityId = _entityId;
            ExecuteRule();
        }

        private Configuration Configuration { get; }
        public string RuleName { get; } = "GovernmentUnlistedFinancial";
        public override void ExecuteRule()
        {

            var directorAffiliations = new List<dynamic>();
            foreach (var companyDirector in _categorizationRulesParameters.DirectorsList)
            {
                dynamic c = new ExpandoObject();
                c.Age = companyDirector.AgeOfCompany;
                c.Capital = companyDirector.PaidUpCapital.ToString();
                directorAffiliations.Add(c);
            }
            if (Configuration == null)
                throw new NotFoundException("Configuration is not set.");
            List<CompanyDBDetail> groupConfiguration;
            if (!Configuration.TryGetValue("application", out groupConfiguration))
                throw new NotFoundException($"The {RuleName} not found");

            CompanyDBDetail extractedScoreCardRules = new CompanyDBDetail();
            if (!string.IsNullOrWhiteSpace(RuleName))
                extractedScoreCardRules = groupConfiguration.Where(r => r.Name == RuleName).FirstOrDefault();

            var payload = new { DirectorAffiliations = directorAffiliations, AgeOfCompany = _categorizationRulesParameters.AgeOfCompany, PaidUpCapital = _categorizationRulesParameters.PaidUpCapital, AuthCapital = _categorizationRulesParameters.AuthorizedCapital, Revenue = _categorizationRulesParameters.Revenue, Profit = _categorizationRulesParameters.Profit, IsState = _categorizationRulesParameters.IsState };
            var executionResult = DecisionEngineService.Execute<dynamic, CompanyCategoryResult>(extractedScoreCardRules.Rule.RuleName,new { payload = payload });
            var result = executionResult;
            if (result.Status == true)
            {
                if (result.Result.ToString().Equals("A+") || result.Result.ToString().Equals("A") || result.Result.ToString().Equals("B"))
                {
                    _ruleResult.Result = "Unclassified";
                    _ruleResult.RuleName = nameof(GovtUnlistedFinancialScoreCard);        
                }
                else{
                    _ruleResult.Result = result.Result.ToString();
                    _ruleResult.RuleName = nameof(GovtUnlistedFinancialScoreCard);
                }
                
            }
        }

    }
}