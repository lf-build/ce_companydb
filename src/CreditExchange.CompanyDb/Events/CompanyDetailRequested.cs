﻿using LendFoundry.SyndicationStore.Events;

namespace CreditExchange.CompanyDb.Events
{
    public class CompanyDetailRequested : SyndicationCalledEvent
    {
    }
}
