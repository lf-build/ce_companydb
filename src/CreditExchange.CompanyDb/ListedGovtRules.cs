﻿using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using CreditExchange.CompanyDb.Abstractions;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Foundation.Services;

namespace CreditExchange.CompanyDb
{
    public class ListedGovtRules : CompanyCategorizationRuleBase
    {
        private readonly CompanyCategorizationRulesParameters _categorizationRulesParameters;
        private readonly RuleResult _ruleResult;

        public ListedGovtRules(CompanyCategorizationRulesParameters categorizationRulesParameters, RuleResult ruleResult)
        {
            _categorizationRulesParameters = categorizationRulesParameters;
            _ruleResult = ruleResult;
        }
        public override void ExecuteRule()
        {
            if (_categorizationRulesParameters.Navratna)
            {
                _ruleResult.Result = "A";
                _ruleResult.RuleName = nameof(ListedGovtRules);
                return;
            }
            SetNextRule(new ListedGovtBankCategoryRule(_categorizationRulesParameters, _ruleResult));
        }
    }

    public sealed class ListedGovtBankCategoryRule : CompanyCategorizationRuleBase
    {
        private readonly RuleResult _ruleResult;
        private readonly CompanyCategorizationRulesParameters _categorizationRulesParameters;

        public ListedGovtBankCategoryRule(CompanyCategorizationRulesParameters categorizationRulesParameters, RuleResult ruleResult)
        {
           
            _categorizationRulesParameters = categorizationRulesParameters;
            _ruleResult = ruleResult;
            ExecuteRule();
        }

        public override void ExecuteRule()
        {
            if (!string.IsNullOrWhiteSpace(_categorizationRulesParameters.BankCategory))
            {
                _ruleResult.Result = _categorizationRulesParameters.BankCategory;
                _ruleResult.RuleName = nameof(ListedGovtBankCategoryRule);
                return;
            }
            SetNextRule(new ListedGovtScoreCardRule(_categorizationRulesParameters, _ruleResult));
        }
    }

    public class ListedGovtScoreCardRule : CompanyCategorizationRuleBase
    {
        private readonly CompanyCategorizationRulesParameters _categorizationRulesParameters;
        private readonly RuleResult _ruleResult;

        public ListedGovtScoreCardRule(CompanyCategorizationRulesParameters categorizationRulesParameters, RuleResult ruleResult)
        {
            _categorizationRulesParameters = categorizationRulesParameters;
            _ruleResult = ruleResult;
            Configuration = ServiceLocator.ServiceProvider.GetService<Configuration>();
            DecisionEngineService = ServiceLocator.ServiceProvider.GetService<IDecisionEngineService>();
            ExecuteRule();
        }
        private IDecisionEngineService DecisionEngineService;
        private Configuration Configuration { get; }
        public string RuleName { get; } = "GovernmentListed";
        public sealed override  void ExecuteRule()
        {          

            var directorAffiliations = new List<dynamic> ();
            foreach (var companyDirector in _categorizationRulesParameters.DirectorsList)
            {
                dynamic c = new ExpandoObject();
                c.Age = companyDirector.AgeOfCompany;
                c.Capital = companyDirector.PaidUpCapital.ToString(CultureInfo.InvariantCulture);
                directorAffiliations.Add(c);
            }

            if (Configuration == null)
                throw new NotFoundException("Configuration is not set.");
            List<CompanyDBDetail> groupConfiguration;
            if (!Configuration.TryGetValue("application", out groupConfiguration))
                throw new NotFoundException($"The {RuleName} not found");

            CompanyDBDetail extractedScoreCardRules = new CompanyDBDetail();
            if (!string.IsNullOrWhiteSpace(RuleName))
                extractedScoreCardRules = groupConfiguration.Where(r => r.Name == RuleName).FirstOrDefault();

            var payload = new { DirectorAffiliations = directorAffiliations, AgeOfCompany = _categorizationRulesParameters.AgeOfCompany, PaidUpCapital = _categorizationRulesParameters.PaidUpCapital, AuthCapital = _categorizationRulesParameters.AuthorizedCapital, Revenue = _categorizationRulesParameters.Revenue, Profit = _categorizationRulesParameters.Profit, IsState = _categorizationRulesParameters.IsState };
            var executionResult = DecisionEngineService.Execute<dynamic, CompanyCategoryResult>(extractedScoreCardRules.Rule.RuleName,new { payload = payload });
            var result = executionResult;
            if (result.Status == true)
            {
                _ruleResult.Result = result.Result.ToString();
                _ruleResult.RuleName = nameof(ListedGovtScoreCardRule);
            }
        }
    }
}