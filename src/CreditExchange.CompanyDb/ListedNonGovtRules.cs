﻿using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using CreditExchange.CompanyDb.Abstractions;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Foundation.Services;

namespace CreditExchange.CompanyDb
{
    public sealed class ListedNonGovtRules : CompanyCategorizationRuleBase
    {
        private readonly RuleResult _ruleResult;
        private readonly CompanyCategorizationRulesParameters _categorizationRulesParameters;
        public ListedNonGovtRules( CompanyCategorizationRulesParameters categorizationRulesParameters, RuleResult ruleResult)
        {
            _ruleResult = ruleResult;
            _categorizationRulesParameters = categorizationRulesParameters;
        }
        public override void ExecuteRule()
        {
            if (!string.IsNullOrWhiteSpace(_categorizationRulesParameters.BankCategory))
            {
                _ruleResult.Result = _categorizationRulesParameters.BankCategory;
                _ruleResult.RuleName = nameof(ListedNonGovtRules);
                return;
            }
            SetNextRule(new ListedCompanyScoreCardRule(_categorizationRulesParameters, _ruleResult));
        }
    }

    public sealed class ListedCompanyScoreCardRule : CompanyCategorizationRuleBase
    {
        private readonly CompanyCategorizationRulesParameters _categorizationRulesParameters;
        private readonly RuleResult _ruleResult;
        private IDecisionEngineService DecisionEngineService;
        public ListedCompanyScoreCardRule(CompanyCategorizationRulesParameters categorizationRulesParameters, RuleResult ruleResult)
        {
            _categorizationRulesParameters = categorizationRulesParameters;
            _ruleResult = ruleResult;
            DecisionEngineService = ServiceLocator.ServiceProvider.GetService<IDecisionEngineService>();
            Configuration = ServiceLocator.ServiceProvider.GetService<Configuration>();
            ExecuteRule();
        }

        private Configuration Configuration { get; }
        public string RuleName { get; } = "ListedCompany";
        public override  void ExecuteRule()
        {

            var directorAffiliations = new List<dynamic>();
            foreach (var companyDirector in _categorizationRulesParameters.DirectorsList)
            {
                dynamic c = new ExpandoObject();
                c.Age = companyDirector.AgeOfCompany;
                c.Capital = companyDirector.PaidUpCapital.ToString(CultureInfo.InvariantCulture);
                directorAffiliations.Add(c);
            }

            if (Configuration == null)
                throw new NotFoundException("Configuration is not set.");
            List<CompanyDBDetail> groupConfiguration;
            if (!Configuration.TryGetValue("application", out groupConfiguration))
                throw new NotFoundException($"The {RuleName} not found");

            CompanyDBDetail extractedScoreCardRules = new CompanyDBDetail();
            if (!string.IsNullOrWhiteSpace(RuleName))
                extractedScoreCardRules = groupConfiguration.Where(r => r.Name == RuleName).FirstOrDefault();
            var payload = new { DirectorAffiliations = directorAffiliations, AgeOfCompany = _categorizationRulesParameters.AgeOfCompany, PaidUpCapital = _categorizationRulesParameters.PaidUpCapital, AuthCapital = _categorizationRulesParameters.AuthorizedCapital, Revenue = _categorizationRulesParameters.Revenue, Profit = _categorizationRulesParameters.Profit, IsState = _categorizationRulesParameters.IsState };
            var executionResult = DecisionEngineService.Execute<dynamic, CompanyCategoryResult>(extractedScoreCardRules.Rule.RuleName,new { payload = payload });
            var result = executionResult;
            // var result = res.employerCategory.categoryCalculations.listedCompany;
            if (result.Status == true)
            {
                _ruleResult.Result = result.Result.ToString();
                _ruleResult.RuleName = nameof(ListedCompanyScoreCardRule);
            }
        }
      
        //  public IEvaluationService ExpressionService { get; private set; }
    }
}