﻿using System.Collections.Generic;
using System.Dynamic;
using CreditExchange.CompanyDb.Abstractions;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System.Globalization;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Foundation.Services;

namespace CreditExchange.CompanyDb
{
    public class UnlistedNonGovtRules : CompanyCategorizationRuleBase
    {
        private readonly string _entityId;
        private readonly string _entityType;
        private readonly CompanyCategorizationRulesParameters _categorizationRulesParameters;
        private readonly RuleResult _ruleResult;



        public UnlistedNonGovtRules(CompanyCategorizationRulesParameters categorizationRulesParameters, RuleResult ruleResult, string entityType, string entityId)
        {
            _categorizationRulesParameters = categorizationRulesParameters;
            _ruleResult = ruleResult;
            _entityType = entityType;
            _entityId = entityId;
        }

        public override void ExecuteRule()
        {
            if (!string.IsNullOrWhiteSpace(_categorizationRulesParameters.BankCategory))
            {
                _ruleResult.Result = _categorizationRulesParameters.BankCategory;
                _ruleResult.RuleName = nameof(UnlistedNonGovtRules);
                return;
            }
            SetNextRule(new UnlistedNonGovtPvtCompanyScoreCardRule(_categorizationRulesParameters, _ruleResult, _entityType, _entityId));
        }
    }

    public sealed class UnlistedNonGovtPvtCompanyScoreCardRule : CompanyCategorizationRuleBase
    {
        private readonly CompanyCategorizationRulesParameters _categorizationRulesParameters;
        private readonly RuleResult _ruleResult;
        private string _entityType;
        private string _entityId;
        private IDecisionEngineService DecisionEngineService;


        public UnlistedNonGovtPvtCompanyScoreCardRule(CompanyCategorizationRulesParameters categorizationRulesParameters, RuleResult ruleResult, string _entityType, string _entityId)
        {
            _categorizationRulesParameters = categorizationRulesParameters;
            _ruleResult = ruleResult;
            Configuration = ServiceLocator.ServiceProvider.GetService<Configuration>();
            DecisionEngineService = ServiceLocator.ServiceProvider.GetService<IDecisionEngineService>();
            this._entityType = _entityType;
            this._entityId = _entityId;
            ExecuteRule();
        }

        private Configuration Configuration { get; }
        public string RuleName { get; } = "PrivateCompany";

        public override void ExecuteRule()
        {
            var directorAffiliations = new List<dynamic>();

            foreach (var companyDirector in _categorizationRulesParameters.DirectorsList)
            {
                dynamic c = new ExpandoObject();
                c.Age = companyDirector.AgeOfCompany;
                c.Capital = companyDirector.PaidUpCapital.ToString(CultureInfo.InvariantCulture);
                directorAffiliations.Add(c);
            }

            if (Configuration == null)
                throw new NotFoundException("Configuration is not set.");

            List<CompanyDBDetail> groupConfiguration;
            if (!Configuration.TryGetValue("application", out groupConfiguration))
                throw new NotFoundException($"The {RuleName} not found");

            CompanyDBDetail extractedScoreCardRules = new CompanyDBDetail();
            if (!string.IsNullOrWhiteSpace(RuleName))
               extractedScoreCardRules = groupConfiguration.Where(r => r.Name == RuleName).FirstOrDefault();

            var payload = new { DirectorAffiliations = directorAffiliations, AgeOfCompany = _categorizationRulesParameters.AgeOfCompany, PaidUpCapital = _categorizationRulesParameters.PaidUpCapital, AuthCapital = _categorizationRulesParameters.AuthorizedCapital, Revenue = _categorizationRulesParameters.Revenue, Profit = _categorizationRulesParameters.Profit, IsState = _categorizationRulesParameters.IsState };
            var executionResult = DecisionEngineService.Execute<dynamic, CompanyCategoryResult>(extractedScoreCardRules.Rule.RuleName,new { payload = payload });
            var result = executionResult;
            _ruleResult.Result = result.Result.ToString();
            _ruleResult.RuleName = nameof(UnlistedNonGovtPvtCompanyScoreCardRule);
            return;
        }
    }

    public sealed class UnlistedNonGovtPvtCompanyFinancialScoreCard : CompanyCategorizationRuleBase
    {
        private readonly CompanyCategorizationRulesParameters _categorizationRulesParameters;
        private readonly RuleResult _ruleResult;
        private IDecisionEngineService DecisionEngineService;
        private Configuration Configuration { get; }
        public string RuleName { get; } = "PrivateCompanyFinancial";
        public UnlistedNonGovtPvtCompanyFinancialScoreCard(CompanyCategorizationRulesParameters categorizationRulesParameters, RuleResult ruleResult)
        {
            _categorizationRulesParameters = categorizationRulesParameters;
            _ruleResult = ruleResult;
            DecisionEngineService = ServiceLocator.ServiceProvider.GetService<IDecisionEngineService>();
            Configuration = ServiceLocator.ServiceProvider.GetService<Configuration>();
            ExecuteRule();
        }
        //   public IEvaluationService ExpressionService { get; }
        public override void ExecuteRule()
        {           

            var directorAffiliations = new List<dynamic>();
            foreach (var companyDirector in _categorizationRulesParameters.DirectorsList)
            {
                dynamic c = new ExpandoObject();
                c.Age = companyDirector.AgeOfCompany;
                c.Capital = companyDirector.PaidUpCapital.ToString(CultureInfo.InvariantCulture);
                directorAffiliations.Add(c);
            }

            if (Configuration == null)
                throw new NotFoundException("Configuration is not set.");

            List<CompanyDBDetail> groupConfiguration;
            if (!Configuration.TryGetValue("application", out groupConfiguration))
                throw new NotFoundException($"The {RuleName} not found");

            CompanyDBDetail extractedScoreCardRules = new CompanyDBDetail();
            if (!string.IsNullOrWhiteSpace(RuleName))
                extractedScoreCardRules = groupConfiguration.Where(r => r.Name == RuleName).FirstOrDefault();
            var payload = new { DirectorAffiliations = directorAffiliations, AgeOfCompany = _categorizationRulesParameters.AgeOfCompany, PaidUpCapital = _categorizationRulesParameters.PaidUpCapital, AuthCapital = _categorizationRulesParameters.AuthorizedCapital, Revenue = _categorizationRulesParameters.Revenue, Profit = _categorizationRulesParameters.Profit, IsState = _categorizationRulesParameters.IsState };
            var executionResult = DecisionEngineService.Execute<dynamic, CompanyCategoryResult>(extractedScoreCardRules.Rule.RuleName,new { payload = payload });
            var result = executionResult;

            if (result.Status == true)
            {
                 if (result.Result.ToString().Equals("A+") || result.Result.ToString().Equals("A") || result.Result.ToString().Equals("B"))
                {
                    _ruleResult.Result = "Unclassified";
                    _ruleResult.RuleName = nameof(UnlistedNonGovtPvtCompanyScoreCardRule);
                }
                else{
                    _ruleResult.Result = result.Result.ToString();
                   _ruleResult.RuleName = nameof(UnlistedNonGovtPvtCompanyScoreCardRule);
                }
            }
        }
    }
}