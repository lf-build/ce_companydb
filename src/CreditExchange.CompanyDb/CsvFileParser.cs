﻿using CreditExchange.CompanyDb.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CreditExchange.CompanyDb
{
    public class CsvFileParser : ICsvFileParser
    {
        public IEnumerable<Company> ParseCompanyCsvFile(string filePath)
        {
            var companymasterList = new List<Company>();

            var lines = File.ReadAllLines(filePath);
            var csvData = lines.Skip(1).ToList(); // skip the header


            foreach (var line in csvData)
            {
                var columns = GetColumns(line);
                if (!IsRequiredColumnsPresent(line, "company"))
                {
                    throw new InvalidOperationException(
                        $"Invalid columns count found. The expected columns should be 21 but actual are {columns.Length}. The line data is {@"\r\n"} {line}");


                }
                try
                {
                    var companyMaster = new Company
                    {
                        AgeOfCompany = Convert.ToInt32(columns[0]),
                        AuthorisedCapital = !string.IsNullOrEmpty(columns[1]) ? Convert.ToDecimal(columns[1]) : 0,
                        BankCategory = columns[2],
                        Category = columns[3],
                        Cin = columns[4],
                        Name = columns[5],
                        LastYearProfit = !string.IsNullOrEmpty(columns[6]) ? Convert.ToDecimal(columns[6]) : 0,
                        LastYearRevenue = !string.IsNullOrEmpty(columns[7]) ? Convert.ToDecimal(columns[7]) : 0,
                        MoneyControlCompanyName = columns[8],
                        MoneyControlProfit = !string.IsNullOrEmpty(columns[9]) ? Convert.ToDecimal(columns[9]) : 0,
                        MoneyControlRevenue = !string.IsNullOrEmpty(columns[10]) ? Convert.ToDecimal(columns[10]) : 0,
                        MoneyControlUrl = columns[11],
                        NavratnaCategory = columns[12],
                        PaidUpCapital = !string.IsNullOrEmpty(columns[13]) ? Convert.ToDecimal(columns[13]) : 0,
                        StatusOfListing = columns[14],
                        SubCategory = columns[15],
                        Type = columns[16],
                        ZaubaCorpUrl = columns[17],
                        Domain = columns[18],
                        ExperianCategory = columns[19],
                        RblDefaulters = columns[20]

                    };
                    companymasterList.Add(companyMaster);
                }
                catch (Exception exception)
                {

                    throw new InvalidOperationException($"Parsing of company failed for Cin {columns[4]}", exception);
                }

            }


            return companymasterList;
        }

        public IEnumerable<CompanyDirector> ParseDirectorsCsvFile(string filePath)
        {
          
                var directorsList = new List<CompanyDirector>();

            var lines = File.ReadAllLines(filePath);
            var csvData = lines.Skip(1).ToList(); // skip the header



            foreach (var line in csvData)
            {
                var columns = GetColumns(line);
                if (!IsRequiredColumnsPresent(line, "directors"))
                {
                    throw new InvalidOperationException(
                        $"Invalid columns count found. The expected columns should be 5 but actual are {columns.Length}. The line data is {@"\r\n"} {line}");


                }
                try
                {

                    var director = new CompanyDirector
                    {
                        Din = columns[1],
                        AgeOfCompany = Convert.ToInt32(columns[3]),
                        Cin = columns[0],
                        Name = columns[2],
                        PaidUpCapital = !string.IsNullOrEmpty(columns[4]) ? Convert.ToDecimal(columns[4]) : 0


                    };

                    directorsList.Add(director);
                }
                catch (Exception exception)
                {

                    throw new InvalidOperationException($"Parsing of directors failed for Din {columns[1]}", exception);
                }

            }
            return directorsList;
            
           
        }

        private static string[] GetColumns(string line)
        {
            var columns = line.Split(',');

            return columns;
        }

        private static bool IsRequiredColumnsPresent(string line,string fileType)
        {
            var columns = line.Split(',');
            switch (fileType)
            {
                case "company":
                    {
                        return columns.Length == 21;
                    }
                case "directors":
                    {
                        return columns.Length == 5;
                    }
                default:
                    return false;
            }
        }
    }
}
