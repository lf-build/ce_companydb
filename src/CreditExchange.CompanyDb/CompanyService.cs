﻿using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using CreditExchange.CompanyDb.Abstractions;
using System;
using CreditExchange.CompanyDb.Events;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Lookup;
using System.Diagnostics;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;

namespace CreditExchange.CompanyDb
{
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyDbRepository _companyDbRepository;

        private IEventHubClient EventHub { get; }

        private ILogger Logger { get; }
        private ILookupService Lookup { get; }

        public static string ServiceName { get; } = "company-db";

        public CompanyService(ICompanyDbRepository companyDbRepository, IEventHubClient eventHub, ILookupService lookup, ILogger logger)
        {
            _companyDbRepository = companyDbRepository;
            EventHub = eventHub;
            Lookup = lookup;
            Logger = logger;

        }
        public async Task<CompanySearchResponses> SearchByName(string entityType, string entityId, string searchText)
        {
            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("Entity type is require", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Entity identifier is require", nameof(entityId));
            entityType = EnsureEntityType(entityType);
            
            var searchResults = await _companyDbRepository.SearchByName(searchText);

            var searchProjection = searchResults.Select(c => new CompanySearchResponse
            {
                Name = c.Name,
                CompanyCategory = c.Category,
                Cin = c.Cin,
                BankCategory = c.BankCategory,
                NavratnaCategory = c.NavratnaCategory,
                Type = c.Type,
                SubCategory = c.SubCategory,
                ExperianCateogory = c.ExperianCategory,
                RBLDefaulters = c.RblDefaulters,
                BSNot=c.BSNot

            }).OrderBy(c => c.Name).Take(15).ToList();

            var referencenumber = Guid.NewGuid().ToString("N");
            await EventHub.Publish(new CompanySearched
            {
                EntityId = entityId,
                EntityType = entityType,
                ReferenceNumber = referencenumber,
                Response = searchProjection,
                Request = new { searchText },
                Name = ServiceName
            });
            var result = new CompanySearchResponses
            {
                CompanySearchResponse = searchProjection,
                ReferenceNumber = referencenumber
            };

            stopwatch.Stop();
            Logger.Info("Request came to service and took time to execute is : " + stopwatch.Elapsed + " ms");
            return result;
        }
        public async Task<CompanySearchResponses> SearchByNameOrCin(string entityType, string entityId, string searchText)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("Entity type is require", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Entity identifier is require", nameof(entityId));
            entityType = EnsureEntityType(entityType);

            var searchResults = await _companyDbRepository.SearchByNameOrCin(searchText);

            var searchProjection = searchResults.Select(c => new CompanySearchResponse
            {
                Name = c.Name,
                CompanyCategory = c.Category,
                Cin = c.Cin,
                BankCategory = c.BankCategory,
                NavratnaCategory = c.NavratnaCategory,
                Type = c.Type,
                SubCategory = c.SubCategory,
                ExperianCateogory = c.ExperianCategory,
                RBLDefaulters = c.RblDefaulters,
                BSNot=c.BSNot

            }).OrderBy(c => c.Name).Take(15).ToList();

            var referencenumber = Guid.NewGuid().ToString("N");
            await EventHub.Publish(new CompanySearched
            {
                EntityId = entityId,
                EntityType = entityType,
                ReferenceNumber = referencenumber,
                Response = searchProjection,
                Request = new { searchText },
                Name = ServiceName
            });
            var result = new CompanySearchResponses
            {
                CompanySearchResponse = searchProjection,
                ReferenceNumber = referencenumber
            };
            return result;
        }

        public async Task<RuleResult> GetCompanyCategory(string entityType, string entityId, ICompanyWithDirectors companyWithDirectors, string cin)
        {
            var referencenumber = Guid.NewGuid().ToString("N");

            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("Entity type is require", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Entity identifier is require", nameof(entityId));
            entityType = EnsureEntityType(entityType);
            try
            {
                var rulesParams = GetParametersFromCompany(companyWithDirectors);
                var ruleresult = new RuleResult();
                ruleresult.Cin = cin;
                if (companyWithDirectors.Company.StatusOfListing.ToLower() == "unlisted" && companyWithDirectors.Company.Category.ToLower() == "government")
                {
                    CompanyCategorizationRuleBase unlistedGovtRule = new UnlistedGovtRule(rulesParams, ruleresult, entityType, entityId);

                    unlistedGovtRule.ExecuteRule();
                    ruleresult.ReferenceNumber = referencenumber;
                    await PublishEvent(entityType, entityId, referencenumber, companyWithDirectors, ruleresult);
                    return ruleresult;
                }
                if (companyWithDirectors.Company.StatusOfListing.ToLower() == "unlisted" && companyWithDirectors.Company.Category.ToLower() == "non-govt company")
                {
                    CompanyCategorizationRuleBase unlistedNonGovtRule = new UnlistedNonGovtRules(rulesParams, ruleresult, entityType, entityId);
                    unlistedNonGovtRule.ExecuteRule();
                    ruleresult.ReferenceNumber = referencenumber;
                    await PublishEvent(entityType, entityId, referencenumber, companyWithDirectors, ruleresult);
                    return ruleresult;
                }

                if (companyWithDirectors.Company.StatusOfListing.ToLower() == "listed" && companyWithDirectors.Company.Category.ToLower() == "government")
                {
                    CompanyCategorizationRuleBase listedGovtRule = new ListedGovtRules(rulesParams, ruleresult);
                    listedGovtRule.ExecuteRule();
                    ruleresult.ReferenceNumber = referencenumber;
                    await PublishEvent(entityType, entityId, referencenumber, companyWithDirectors, ruleresult);
                    return ruleresult;
                }

                if (companyWithDirectors.Company.StatusOfListing.ToLower() == "listed" && companyWithDirectors.Company.Category.ToLower() == "non-govt company")
                {
                    CompanyCategorizationRuleBase listedNonGovtRole = new ListedNonGovtRules(rulesParams, ruleresult);
                    listedNonGovtRole.ExecuteRule();
                    ruleresult.ReferenceNumber = referencenumber;
                    await PublishEvent(entityType, entityId, referencenumber, companyWithDirectors, ruleresult);
                    return ruleresult;
                }
                // if none of the conditions then default score is applied.
                ruleresult.Result = "D";
                ruleresult.RuleName = "LLP or Partnership";
                await PublishEvent(entityType, entityId, referencenumber, companyWithDirectors, ruleresult);
                ruleresult.ReferenceNumber = referencenumber;
                return ruleresult;
            }
            catch (Exception e)
            {
                var ruleresult = new RuleResult();
                ruleresult.ReferenceNumber = referencenumber;
                ruleresult.Result = "Unclassified";
                ruleresult.Cin = cin;
                await PublishEvent(entityType, entityId, referencenumber, companyWithDirectors, ruleresult);
                throw new Exception("Error occured :" + e.Message);
            }
        }

        public async Task<CompanyWithDirectors> GetCompanyWithDirectors(string entityType, string entityId, string cin)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("Entity type is require", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Entity identifier is require", nameof(entityId));
            entityType = EnsureEntityType(entityType);

            var companyWithDirectors = await _companyDbRepository.GetCompany(cin);
            var referencenumber = Guid.NewGuid().ToString("N");
            await EventHub.Publish(new CompanyDetailRequested
            {
                EntityId = entityId,
                EntityType = entityType,
                ReferenceNumber = referencenumber,
                Response = companyWithDirectors,
                Request = new { cin },
                Name = ServiceName
            });
            companyWithDirectors.ReferenceNumber = referencenumber;
            if (companyWithDirectors == null || companyWithDirectors.Company == null)
            {
                var ruleresult = new RuleResult();
                ruleresult.ReferenceNumber = referencenumber;
                ruleresult.Result = "Unclassified";
                ruleresult.Cin = cin;
                await PublishEvent(entityType, entityId, referencenumber, companyWithDirectors, ruleresult);
                throw new Exception("Company Not Found");
            }
            else if (companyWithDirectors != null)
            {
                if (companyWithDirectors.Directors?.Count == 0)
                {
                    var ruleresult = new RuleResult();
                    ruleresult.ReferenceNumber = referencenumber;
                    ruleresult.Result = "Unclassified";
                    ruleresult.Cin = cin;
                    await PublishEvent(entityType, entityId, referencenumber, companyWithDirectors, ruleresult);
                    throw new Exception("Company Not Found");
                }
            }

            return companyWithDirectors;
        }

        //public async Task<CompanyWithDirectors> GetCompanyWithDirectors(string cin)
        //{
        //    var company = await _companyDbRepository.GetCompany(cin).ConfigureAwait(false);
        //    return company;
        //}

        private static CompanyCategorizationRulesParameters GetParametersFromCompany(ICompanyWithDirectors companyWithDirectors)
        {
            var companycategorizatonrulesParams = new CompanyCategorizationRulesParameters
            {

                BankCategory = companyWithDirectors.Company.BankCategory,
                AgeOfCompany = companyWithDirectors.Company.AgeOfCompany.ToString(),
                AuthorizedCapital = companyWithDirectors.Company.AuthorisedCapital.ToString(CultureInfo.InvariantCulture),
                Navratna = companyWithDirectors.Company.NavratnaCategory != "NA",
                PaidUpCapital = companyWithDirectors.Company.PaidUpCapital.ToString(CultureInfo.InvariantCulture),
                Profit = companyWithDirectors.Company.MoneyControlProfit == 0 ? "0" : companyWithDirectors.Company.MoneyControlProfit.ToString(CultureInfo.InvariantCulture),
                Revenue = companyWithDirectors.Company.MoneyControlRevenue == 0 ? "0" : companyWithDirectors.Company.MoneyControlRevenue.ToString(CultureInfo.InvariantCulture),
                DirectorsList = companyWithDirectors.Directors.ToArray(),
                Cin = companyWithDirectors.Company.Cin,
                IsState = companyWithDirectors.Company.SubCategory.ToLower() == "state"
            };

            return companycategorizatonrulesParams;
        }

        private async Task PublishEvent(string entityType, string entityId, string referenceNumber, ICompanyWithDirectors companyWithDirectors, RuleResult ruleResult)
        {
            await EventHub.Publish(new CompanyDbCategoryRequested
            {
                EntityId = entityId,
                EntityType = entityType,
                ReferenceNumber = referenceNumber,
                Response = ruleResult,
                Request = companyWithDirectors,
                Name = ServiceName
            });
        }

        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}