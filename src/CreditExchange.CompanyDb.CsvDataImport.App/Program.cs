﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Framework.Configuration;
using Microsoft.Framework.DependencyInjection;
using CreditExchange.CompanyDb.Abstractions;
using Microsoft.Framework.OptionsModel;
using System.Threading.Tasks;
using CreditExchange.CompanyDb.Persistence;
using LendFoundry.Foundation.Date;

using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Http.Internal;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Services;

namespace CreditExchange.CompanyDb.CsvDataImport.App
{
    public class Program :DependencyInjection
    {
        //private static IServiceCollection serviceCollection;
        //private static IServiceProvider serviceProvider;
        public   void Main(string[] args)
        {
            //  serviceCollection = new ServiceCollection();
            //  serviceCollection.AddSingleton<ICompanyDbRepository, CompanyDbRepository>();
            //  serviceCollection.Add(ServiceDescriptor.Transient(typeof(IOptions<>), typeof(OptionsManager<>)));

            //   var configurationBuilder = new ConfigurationBuilder().AddJsonFile("mongodbconfig.json");
            //    Configuration = configurationBuilder.Build();
            //    serviceCollection.Configure<MongoDbSettings>(Configuration);
            //    serviceProvider = serviceCollection.BuildServiceProvider();
            StartImportAsync().Wait();
             // Search("a").Wait();
           // ServiceLocator.ServiceProvider = Provider;

           
            var directors = new List<CompanyDirector>
            {
                new CompanyDirector
                {
                    AgeOfCompany = 10,
                    Cin = "XXXXX",
                    Din = "AAA",
                    Name = "Mr X",
                    PaidUpCapital = 1000000
                },
                new CompanyDirector
                {
                    AgeOfCompany = 12,
                    Cin = "XXXXX",
                    Din = "BBB",
                    Name = "Mr Y",
                    PaidUpCapital = 2000000
                }
            };
         // GetCompanies("U18109DL2008PTC177167").Wait();
          //  var rulesParams = new CompanyCategorizationRulesParameters { Navratna = false, BankCategory = "",AgeOfCompany = "16",AuthorizedCapital = "50000000",DirectorsList = directors.ToArray(),PaidUpCapital = "7000000"};
           // var ruleresult = new RuleResult();
         //   CompanyCategorizationRuleBase unlistedGovtRule = new UnlistedGovtRule(rulesParams,ruleresult);
           // CompanyCategorizationRuleBase unlistedBankCategoryRule = new UnlistedGovtBankCategoryRule();
           // unlistedGovtRule.SetNextRule(unlistedBankCategoryRule);
            
          //  unlistedGovtRule.CategorizationRulesParameters = rulesParams;
           // unlistedGovtRule.ExecuteRule();
          //  Console.WriteLine(ruleresult.Result);
            Console.ReadLine();
        }

        public static IConfigurationRoot Configuration { get; set; }

        public async Task GetCompanies(string cin)
        {
            var companysvc = Provider.GetService<ICompanyDbRepository>();
            var comapnies = await   companysvc.GetCompany(cin);
           
               // Console.WriteLine(comapnies.Directors.Count);
            

        }

        public async  Task StartImportAsync()
        {
            var loggerFactory = Provider.GetService<ILoggerFactory>();
            var logger = loggerFactory.Create(NullLogContext.Instance);
            try
            {
                logger.Debug("Resolving the services");
                var companyDbRepo = Provider.GetService<ICompanyDbRepository>();
                var csvParser = Provider.GetService<ICsvFileParser>();
                logger.Debug("Resolving of services completed");


                logger.Info($"Parsing of {Environment.GetEnvironmentVariable("CE_CompanyMasterCsvFilePath")} started");
                var companymasterList = csvParser.ParseCompanyCsvFile(Environment.GetEnvironmentVariable("CE_CompanyMasterCsvFilePath"));
                if (companymasterList == null)
                {
                    logger.Info("Parsing of file failed. Skipping the process");
                    return;
                }
                var companies = companymasterList as IList<Company> ?? companymasterList.ToList();
                logger.Info($"Parsing of {Environment.GetEnvironmentVariable("CE_CompanyMasterCsvFilePath")} completed. Total {companies.Count} records parsed");

                logger.Info("Insert started for company");
               
                var result = await companyDbRepo.UpsertCompany(companies).ConfigureAwait(false);
                if (result)
                {
                    logger.Info($"Upsert completed for company. Total of {companies.Count} records processed ");
                }
                

                var directorsList = csvParser.ParseDirectorsCsvFile(Environment.GetEnvironmentVariable("CE_DirectorsCsvFilePath"));
                if (directorsList == null)
                {
                    logger.Info("Parsing of directors failed. Skipping the process");
                    return;
                }
                //// directors import
                
                var directors = directorsList as IList<CompanyDirector> ?? directorsList.ToList();

                logger.Info($"Parsing of {Environment.GetEnvironmentVariable("CE_DirectorsCsvFilePath")} completed. Total {directors.Count} records parsed");

                logger.Info("Insert started for directors");

                 result = await companyDbRepo.InsertDirectors(directors).ConfigureAwait(false);
                if (result)
                {
                    logger.Info($"Insert completed for director. Total of {directors.Count} records processed ");
                }
            }
            catch (AggregateException aggregateException)
            {

                foreach (var aggregateExceptionInnerException in aggregateException.InnerExceptions)
                {
                    logger.Error(aggregateExceptionInnerException.Message, aggregateExceptionInnerException);
                }
            }
            catch (Exception exception)
            {
                logger.Error(exception.Message, exception);
            }
        }

        //public async  Task  Search(string searchText)
        //{
        //   var loggerFactory = Provider.GetService<ILoggerFactory>();
        //    var logger = loggerFactory.Create(NullLogContext.Instance);
        //    try
        //    {
        //        Provider.GetService<ICompanyDbRepository>();
        //        var lookup = Provider.GetService<ICompanyService>();

        //        var searchResult = await lookup.SearchByNameOrCin(searchText).ConfigureAwait(false);
        //        foreach (var companyMasterSearchViewModel in searchResult)
        //        {
        //            Console.WriteLine(companyMasterSearchViewModel.Name);
        //            Console.WriteLine(companyMasterSearchViewModel.BankCategory);
        //            Console.WriteLine(companyMasterSearchViewModel.Cin);
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        logger.Error(exception.Message, exception);
        //    }
        //}

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddCaching();
            services.AddMvc();
          //  services.AddSession();
            services.AddCors();
            services.AddTenantTime();
            services.AddTenantService("10.1.1.99", 5005);
            services.AddHttpServiceLogging("ConsoleAppForCompanyDb");
            services.AddTokenHandler();
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            //services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddTransient<ICsvFileParser, CsvFileParser>();
           // services.AddSingleton<IServiceLookUp>(provider => new ServiceLocator(provider)); 
            services.AddSingleton<IMongoConfiguration>(provider => GetMongoConfiguration());
            services.AddSingleton<ICompanyMasterDbContext, CompanyMasterDbContext>();
            services.AddSingleton<ICompanyDbRepository, CompanyDbRepository>();
            services.AddTransient<ICompanyService, CompanyService>();
//
  //          services.AddExpressionParserServices("10.1.1.99", 5000);

            services.Add(ServiceDescriptor.Transient(typeof(IOptions<>), typeof(OptionsManager<>)));
            services.AddServiceLogging("Company", NullLogContext.Instance);
            var configurationBuilder = new ConfigurationBuilder().AddJsonFile("mongodbconfig.json");
            Configuration = configurationBuilder.Build();
            services.Configure<MongoDbSettings>(Configuration);
            return services;
        }

        private static IMongoConfiguration GetMongoConfiguration()
        {
           
           // var mongodbsettingsOptions = Provider.GetService<IOptions<MongoDbSettings>>();
           // var mongoDbSettings = mongodbsettingsOptions.Value;
            return new MongoConfiguration(Environment.GetEnvironmentVariable("CE_MongoConnectionString"), Environment.GetEnvironmentVariable("CE_MongoDatabase"));

        }
    }
}

