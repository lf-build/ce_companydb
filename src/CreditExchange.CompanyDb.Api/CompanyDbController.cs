﻿
using  Microsoft.AspNetCore.Mvc;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Logging;
using System;
using System.Threading.Tasks;
using CreditExchange.CompanyDb.Abstractions;

namespace CreditExchange.CompanyDb.Api
{
    [Route("/")]
    public class CompanyDbController : ExtendedController
    {
        #region Declarations

        private ICompanyService CompanyService { get; }
        private ILogger Logger { get; }


        #endregion

        #region Constructor

        public CompanyDbController(ICompanyService companyLookUpService,ILogger logger):base(logger)
        {
            if (logger == null)
            {
                throw new ArgumentNullException(nameof(logger));
            }
            Logger = logger;
            {
                if (companyLookUpService == null)
                {
                    throw new ArgumentNullException(nameof(companyLookUpService));
                }
                   

                CompanyService = companyLookUpService;
            }
        }

        #endregion

        [HttpGet("{entityType}/{entityId}/company/search/{s}")]
        [ProducesResponseType(typeof(CreditExchange.CompanyDb.Abstractions.CompanySearchResponses), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]

        public async Task<IActionResult> SearchAsync(string entityType, string entityId, string s)
        {
            try
            {
              var decodedString = System.Net.WebUtility.UrlDecode(s);
               return  await ExecuteAsync(async () => Ok(await CompanyService.SearchByNameOrCin(entityType, entityId, decodedString)));
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        [HttpGet("{entityType}/{entityId}/company/search/byname/{s}")]
        [ProducesResponseType(typeof(CreditExchange.CompanyDb.Abstractions.CompanySearchResponses), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> SearchbyName(string entityType, string entityId, string s)
        {
            try
            {
                var decodedString = System.Net.WebUtility.UrlDecode(s);
                return await ExecuteAsync(async () => Ok(await CompanyService.SearchByName(entityType, entityId, decodedString)));
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        [HttpGet("{entityType}/{entityId}/company/category/{cin}")]
        [ProducesResponseType(typeof(CreditExchange.CompanyDb.Abstractions.RuleResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetCompanyCategory(string entityType, string entityId, string cin)
        {
            try
            {
                if (string.IsNullOrEmpty(cin))
                {
                    return Execute(() => Ok("Cin is required"));
                }
                
                var companyWithDirectors = await CompanyService.GetCompanyWithDirectors(entityType, entityId, cin);
                return await ExecuteAsync(async () => Ok(await CompanyService.GetCompanyCategory(entityType, entityId, companyWithDirectors,cin)));
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message,exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("{entityType}/{entityId}/company/directors/{cin}")]
        [ProducesResponseType(typeof(CreditExchange.CompanyDb.Abstractions.CompanyWithDirectors), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetCompanyWithDirectors(string entityType, string entityId, string cin)
        {
            try
            {
               return await ExecuteAsync(async () => Ok(await CompanyService.GetCompanyWithDirectors(entityType, entityId, cin)));
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }
    }
}
