﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.CompanyDb.Api
{
    public static class Settings
    {
        public static string ServiceName { get; } = "company";
        private static string Prefix { get; } = ServiceName.ToUpper();
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");
        public static ServiceSettings LookupService { get; } = new ServiceSettings($"{Prefix}_LOOKUPSERVICE", "lookupservice");
        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static ServiceSettings RulesEngine { get; } = new ServiceSettings($"{Prefix}_RULESENGINE", "rulesengine");
        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "companydb");
        public static ServiceSettings Identity { get; } = new ServiceSettings($"{Prefix}_IDENTITY", "identity");
        public static ServiceSettings Notification { get; } = new ServiceSettings($"{Prefix}_NOTIFICATION", "notification");
        public static ServiceSettings DecisionEngine { get; } = new ServiceSettings($"{Prefix}_DECISIONENGINE_HOST", "decision-engine", $"{Prefix}_DECISIONENGINE_PORT");

        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
    }
}
