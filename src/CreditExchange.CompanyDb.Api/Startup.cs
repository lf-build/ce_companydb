﻿using CreditExchange.CompanyDb.Abstractions;
using CreditExchange.CompanyDb.Persistence;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using LendFoundry.EventHub.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Configuration;

namespace CreditExchange.CompanyDb.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "CompanyDb"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "CreditExchange.CompanyDb.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddConfigurationService<Configuration>(Settings.Configuration.Host,Settings.Configuration.Port,Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddLookupService(Settings.LookupService.Host, Settings.LookupService.Port);
            services.AddDecisionEngine(Settings.DecisionEngine.Host, Settings.DecisionEngine.Port);
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<Configuration>>().Get());
            services.AddLookupService(Settings.LookupService.Host, Settings.LookupService.Port);
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddSingleton<ICompanyMasterDbContext, CompanyMasterDbContext>();
            services.AddSingleton<ICompanyDbRepository, CompanyDbRepository>();
        
            services.AddTransient<ICompanyService, CompanyService>();
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
            var serviceProvider = services.BuildServiceProvider();
            ServiceLocator.ServiceProvider = serviceProvider;

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "CompanyDb Service");
            });

            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            //app.UseMiddleware<RequestLoggingMiddleware>();
            app.UseMvc();
            //app.UseEventHub();
            app.UseHealthCheck();
        }
    }
}

