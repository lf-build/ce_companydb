﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CreditExchange.CompanyDb.Abstractions;
using MongoDB.Bson;
using MongoDB.Driver;
using LendFoundry.Foundation.Logging;
using System.Diagnostics;

namespace CreditExchange.CompanyDb.Persistence
{

    public class CompanyDbRepository : ICompanyDbRepository
    {
        private readonly ICompanyMasterDbContext _companyMasterDbContext;
        private ILogger Logger { get; }

        public CompanyDbRepository(ICompanyMasterDbContext companyMasterDbContext, ILogger logger)
        {
            _companyMasterDbContext = companyMasterDbContext;
            Logger = logger;
        }
        public async Task<bool> UpsertCompany(IEnumerable<Company> companyMasterList)
        {
            var masterList = companyMasterList as IList<Company> ?? companyMasterList.ToList();
            await _companyMasterDbContext.CompanyMasterCollection.DeleteManyAsync(c => c.Id != string.Empty)
                    .ConfigureAwait(false);
            foreach (var company in masterList)
            {
                company.Id = ObjectId.GenerateNewId(DateTime.Now).ToString();
                await _companyMasterDbContext.CompanyMasterCollection.InsertOneAsync(company)
                  .ConfigureAwait(false);
            }
          
            return true;
        }

        public async Task<bool> InsertDirectors(IEnumerable<CompanyDirector> companyDirectors)
        {
            await _companyMasterDbContext.CompanyDirectorCollection.DeleteManyAsync(d => d.Cin != string.Empty)
                    .ConfigureAwait(false);

            foreach (var companyDirector in companyDirectors)
            {
                companyDirector.Id = ObjectId.GenerateNewId(DateTime.Now).ToString();
                await _companyMasterDbContext.CompanyDirectorCollection.InsertOneAsync(companyDirector)
                  .ConfigureAwait(false);
            }
          
            return true;
        }





        public Task<IQueryable<Company>> SearchByName(string searchText)
        {
            // execute the query on separate thread to avoid any blocking io 
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            searchText = searchText.ToLower();

            return Task.Run(() =>
            {

                var searchResults = _companyMasterDbContext.CompanyMasterCollection.AsQueryable().
                    Where(c => c.Name.ToLower().StartsWith(searchText)).Take(5);

                stopwatch.Stop();
                Logger.Info("Request came to repository and took time to execute is : " + stopwatch.Elapsed + " ms");

                return searchResults;
            });


        }
        public Task<IQueryable<Company>> SearchByNameOrCin(string searchText)
        {
            // execute the query on separate thread to avoid any blocking io 



            return Task.Run(() =>
            {
                var searchResults = _companyMasterDbContext.CompanyMasterCollection.AsQueryable().
                    Where(c => c.Name.ToLower().StartsWith(searchText.ToLower())
                               ||
                               c.Cin.ToLower().StartsWith(searchText.ToLower())).Take(50);
                return searchResults;
            });


        }

        public async Task<CompanyWithDirectors> GetCompany(string cin)
        {
         
            var company = await _companyMasterDbContext.CompanyMasterCollection.FindAsync(c => c.Cin == cin).ConfigureAwait(false);


            var directors = await _companyMasterDbContext.CompanyDirectorCollection.FindAsync(d => d.Cin == cin);
                   
                    
            var companywithDirectors = new CompanyWithDirectors
            {
                Company = await company.FirstOrDefaultAsync().ConfigureAwait(false),
                Directors = await directors.ToListAsync().ConfigureAwait(false)
            };
            return companywithDirectors;
         



        }

    }
}




