﻿using System;
using LendFoundry.Foundation.Logging;
using MongoDB.Driver.Core.Events;

namespace CreditExchange.CompanyDb.Persistence
{
    public class MongoDbDiganostics : IEventSubscriber
    {
        private readonly ILoggerFactory _loggerFactory;
        private readonly ReflectionEventSubscriber _Subscriber;

        public MongoDbDiganostics(ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
            _Subscriber = new ReflectionEventSubscriber(this);
        }

        public MongoDbDiganostics()
        {
           // _loggerFactory = new ServiceLoggerFactory();
            _Subscriber = new ReflectionEventSubscriber(this);
        }

        public bool TryGetEventHandler<TEvent>(out Action<TEvent> handler)
        {
            return _Subscriber.TryGetEventHandler(out handler);
        }

        public void Handle(CommandStartedEvent started)
        {
           // var logger = _loggerFactory.Create(NullLogContext.Instance);
            if (started.DatabaseNamespace.DatabaseName == "companydb")
            {
                Console.WriteLine($"Commad name is {started.CommandName} Connection Id is {started.ConnectionId} Database is {started.DatabaseNamespace.DatabaseName}");
            }
           


        }
        }
    }
