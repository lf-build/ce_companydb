﻿using CreditExchange.CompanyDb.Abstractions;
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;

namespace CreditExchange.CompanyDb.Persistence
{
    public class CompanyMasterDbContext : ICompanyMasterDbContext
    {
        public CompanyMasterDbContext(IMongoConfiguration mongoConfiguration)
        {
            //var pack = new ConventionPack
            //    {
            //        new EnumRepresentationConvention(BsonType.String)
            //    };

            //ConventionRegistry.Register("EnumStringConvention", pack, t => true);
            var settings = MongoClientSettings.FromUrl(new MongoUrl(mongoConfiguration.ConnectionString));
           // settings.ClusterConfigurator = builder => builder.Subscribe(new MongoDbDiganostics());
            var client = new MongoClient(settings);
            Database = client.GetDatabase(mongoConfiguration.Database);
            CompanyMasterCollection = Database.GetCollection<Company>("companymaster");
            CompanyDirectorCollection = Database.GetCollection<CompanyDirector>("companydirectors");
            var indexes =CompanyMasterCollection.Indexes.CreateOneAsync((Builders<Company>.IndexKeys.Ascending(_ => _.Cin)));
        }

        public IMongoDatabase Database { get; }
        public IMongoCollection<Company> CompanyMasterCollection { get; }

        public IMongoCollection<CompanyDirector> CompanyDirectorCollection { get; }


    }
}