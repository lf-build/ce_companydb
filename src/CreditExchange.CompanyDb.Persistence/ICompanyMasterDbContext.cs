﻿using CreditExchange.CompanyDb.Abstractions;
using MongoDB.Driver;

namespace CreditExchange.CompanyDb.Persistence
{
    public interface ICompanyMasterDbContext
    {
        IMongoDatabase Database { get; }

        IMongoCollection<Company> CompanyMasterCollection { get; }
        IMongoCollection<CompanyDirector> CompanyDirectorCollection { get; }
    }
}