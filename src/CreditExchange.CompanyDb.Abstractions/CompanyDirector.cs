﻿namespace CreditExchange.CompanyDb.Abstractions
{
    public class CompanyDirector : ICompanyDirector
    {
        public string Id { get; set; }

        public string Cin { get; set; }

        public string Din { get; set; }
        public string Name { get; set; }
        public int AgeOfCompany { get; set; }
        public decimal PaidUpCapital { get; set; }
    }
}