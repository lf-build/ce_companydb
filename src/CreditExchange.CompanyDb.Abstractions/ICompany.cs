﻿namespace CreditExchange.CompanyDb.Abstractions
{
  public interface ICompany
    {
         string Id { get; set; }
        string Cin { get; set; }
         string Name { get; set; }

        string Type { get; set; }

        string StatusOfListing { get; set; }

        string Category { get; set; }

        string SubCategory { get; set; }

        decimal AuthorisedCapital { get; set; }

        decimal PaidUpCapital { get; set; }

        int AgeOfCompany { get; set; }

        decimal LastYearRevenue { get; set; }

        decimal LastYearProfit { get; set; }

        string MoneyControlCompanyName { get; set; }

        decimal MoneyControlRevenue { get; set; }

        decimal MoneyControlProfit { get; set; }

        string MoneyControlUrl { get; set; }

        string ZaubaCorpUrl { get; set; }

        string NavratnaCategory { get; set; }

        string BankCategory { get; set; }

        string Domain { get; set; }

        string ExperianCategory { get; set; }

        string RblDefaulters { get; set; }
         string BSNot { get; set; }
    }
}
