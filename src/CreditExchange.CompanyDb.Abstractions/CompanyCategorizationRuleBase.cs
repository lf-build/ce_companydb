﻿using System.Threading.Tasks;

namespace CreditExchange.CompanyDb.Abstractions
{
    public abstract class CompanyCategorizationRuleBase
    {
        protected CompanyCategorizationRuleBase NextRule;

        public  void SetNextRule(CompanyCategorizationRuleBase nextRule)
        {
            NextRule = nextRule;
          //  ExecuteRule();
        }

      
        public abstract void ExecuteRule();

    }
}