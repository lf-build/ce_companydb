﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.CompanyDb.Abstractions
{
    public class CompanyCategoryResult : ICompanyCategoryResult
    {
        public bool Status { get; set; }
        public string Result { get; set; }
        public string Exception { get; set; }
        public Dictionary<string, object> Data { get; set; }
    }
}
