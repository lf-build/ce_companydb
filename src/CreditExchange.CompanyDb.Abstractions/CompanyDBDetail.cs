﻿

namespace CreditExchange.CompanyDb
{
    public class CompanyDBDetail: ICompanyDBDetail
    {
        public string Name { get; set; }
        public Rule Rule { get; set; }
    }
}
