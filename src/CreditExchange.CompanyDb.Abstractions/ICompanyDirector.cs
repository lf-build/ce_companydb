﻿namespace CreditExchange.CompanyDb.Abstractions
{
   public interface ICompanyDirector
    {
        string Cin { get; set; }

         string Din { get; set; }

         string Name { get; set; }

        int AgeOfCompany { get; set; }

        decimal PaidUpCapital { get; set; }

    }
}
