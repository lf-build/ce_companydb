﻿namespace CreditExchange.CompanyDb.Abstractions
{
    public interface ICompanyCategorizationRulesEngine
    {
        void Execute();
    }
}