﻿namespace CreditExchange.CompanyDb.Abstractions
{
    public interface ICompanySearchResponse
    {
        string Cin { get; set; }
        string CompanyCategory { get; set; }
        string NavratnaCategory { get; set; }
        string Name { get; set; }
        string BankCategory { get; set; }

        string Type { get; set; }
        string SubCategory { get; set; }
        string ExperianCateogory { get; set; }
        string RBLDefaulters { get; set; }
        string BSNot { get; set; }
    }
}