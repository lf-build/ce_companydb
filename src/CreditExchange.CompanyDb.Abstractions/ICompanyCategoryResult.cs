﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.CompanyDb.Abstractions
{
   public interface ICompanyCategoryResult
    {
        bool Status { get; set; }
        string Result { get; set; }
        string Exception { get; set; }
        Dictionary<string, object> Data { get; set; }
    }
}
