﻿using System.Collections.Generic;

namespace CreditExchange.CompanyDb.Abstractions
{
    public class CompanySearchResponses : ICompanySearchResponses
    {
        public IList<CompanySearchResponse> CompanySearchResponse { get; set; }
        public string ReferenceNumber { get; set; }
    }
}