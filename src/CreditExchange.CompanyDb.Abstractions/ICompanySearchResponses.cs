﻿using System.Collections.Generic;

namespace CreditExchange.CompanyDb.Abstractions
{
    public interface ICompanySearchResponses
    {
        IList<CompanySearchResponse> CompanySearchResponse { get; set; }
        string ReferenceNumber { get; set; }
    }
}