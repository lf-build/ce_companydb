﻿namespace CreditExchange.CompanyDb.Abstractions
{
    public class Company :  ICompany
    {
        public string Id { get; set; }
        public int AgeOfCompany
        {
            get;set;
        }

        public decimal AuthorisedCapital
        {
            get; set;
        }

        public string BankCategory
        {
            get; set;
        }

        public string Domain { get; set; }
        public string ExperianCategory { get; set; }
        public string RblDefaulters { get; set; }
        public string BSNot { get; set; }

        public string Category
        {
            get; set;
        }


        public string Cin
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public decimal LastYearProfit
        {
            get; set;
        }

        public decimal LastYearRevenue
        {
            get; set;
        }

        public string MoneyControlCompanyName
        {
            get; set;
        }

        public decimal MoneyControlProfit
        {
            get; set;
        }

        public decimal MoneyControlRevenue
        {
            get; set;
        }

        public string MoneyControlUrl
        {
            get; set;
        }

        public string NavratnaCategory
        {
            get; set;
        }

        public decimal PaidUpCapital
        {
            get; set;
        }

        public string StatusOfListing
        {
            get; set;
        }

        public string SubCategory
        {
            get; set;
        }

        public string Type
        {
            get; set;
        }

        public string ZaubaCorpUrl
        {
            get; set;
        }

    }
}
