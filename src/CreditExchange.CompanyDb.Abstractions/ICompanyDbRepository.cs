﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.CompanyDb.Abstractions
{
    public interface ICompanyDbRepository
    {
        Task<bool> UpsertCompany(IEnumerable<Company> companyMasterList);
        Task<bool> InsertDirectors(IEnumerable<CompanyDirector> companyDirectors);
       Task<IQueryable<Company>> SearchByNameOrCin(string searchText);

        Task<CompanyWithDirectors> GetCompany(string cin);
        Task<IQueryable<Company>> SearchByName(string searchText);
    }
}
