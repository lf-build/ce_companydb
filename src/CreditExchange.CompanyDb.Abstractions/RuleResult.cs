﻿namespace CreditExchange.CompanyDb.Abstractions
{
    public class RuleResult
    {
        public string RuleName { get; set; }

        public string Result { get; set; }

        public string ReferenceNumber { get; set; }

        public string Cin { get; set; }
    }
}