﻿using System.Collections.Generic;

namespace CreditExchange.CompanyDb.Abstractions
{
    public interface ICompanyWithDirectors
    {
        IList<CompanyDirector> Directors { get; set; }
        Company Company { get; set; }
    }
}