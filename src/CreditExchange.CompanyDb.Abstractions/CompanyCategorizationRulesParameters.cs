﻿using System.Collections.Generic;

namespace CreditExchange.CompanyDb.Abstractions
{
    public class CompanyCategorizationRulesParameters
    {
        public bool Navratna { get; set; }
        public string BankCategory { get; set; }
        public IList<CompanyDirector> DirectorsList { get; set; }
        public string AgeOfCompany { get; set; }
        public string PaidUpCapital { get; set; }
        public string AuthorizedCapital { get; set; }
        public string Revenue { get; set; }
        public string Profit { get; set; }
        public string Cin { get; set; }
        public bool IsState { get; set; }
    }
}