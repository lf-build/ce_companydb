﻿namespace CreditExchange.CompanyDb.Abstractions
{
    public class CompanySearchResponse : ICompanySearchResponse
    {
        public string Cin { get; set; }

        public string CompanyCategory { get; set; }

        public string NavratnaCategory { get; set; }

        public string Name { get; set; }

        public string BankCategory { get; set; }
        public string Type { get; set; }
        public string SubCategory { get; set; }
        public string ExperianCateogory { get; set; }
        public string RBLDefaulters { get; set; }
        public string BSNot { get; set; }
    }
}