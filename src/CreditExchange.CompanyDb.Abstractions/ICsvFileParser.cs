﻿using System.Collections.Generic;

namespace CreditExchange.CompanyDb.Abstractions
{
    public interface ICsvFileParser
    {
        IEnumerable<Company> ParseCompanyCsvFile(string filePath);

        IEnumerable<CompanyDirector> ParseDirectorsCsvFile(string filePath);
    }
}
