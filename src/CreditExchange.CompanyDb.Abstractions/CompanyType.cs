﻿namespace CreditExchange.CompanyDb.Abstractions
{
    public enum CompanyType
    {
        Public = 1,
        Private = 2,
        LLP = 3

    }
}