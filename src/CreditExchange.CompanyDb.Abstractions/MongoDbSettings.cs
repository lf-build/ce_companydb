﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.CompanyDb.Abstractions
{
    public class MongoDbSettings
    {
        public string Database { get; set; }
        public string MongoConnection { get; set; }
    }
}
