﻿using System.Collections.Generic;

namespace CreditExchange.CompanyDb.Abstractions
{
    public class CompanyWithDirectors : ICompanyWithDirectors
    {
       
        public IList<CompanyDirector> Directors { get; set; }
        public Company Company { get; set; }
        public string ReferenceNumber { get; set; }
    }
}