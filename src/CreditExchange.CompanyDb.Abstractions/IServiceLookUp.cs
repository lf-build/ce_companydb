﻿using System;

namespace CreditExchange.CompanyDb.Abstractions
{
    public interface IServiceLookUp
    {
        IServiceProvider ServiceProvider { get; }
    }
}