﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CreditExchange.CompanyDb.Abstractions
{
   public interface ICompanyService
   {
       Task<CompanySearchResponses> SearchByNameOrCin(string entityType, string entityId, string searchText);

       Task<RuleResult> GetCompanyCategory(string entityType, string entityId, ICompanyWithDirectors companyWithDirectors, string cin);

       Task<CompanyWithDirectors> GetCompanyWithDirectors(string entityType, string entityId, string cin);

        Task<CompanySearchResponses> SearchByName(string entityType, string entityId, string searchText);
   }
}
