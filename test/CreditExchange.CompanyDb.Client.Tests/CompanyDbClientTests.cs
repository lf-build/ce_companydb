﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using CreditExchange.CompanyDb.Client;
using CreditExchange.CompanyDb.Abstractions;

namespace CreditExchange.CompanyDb.Client.Tests
{
    public class CompanyDbClientTests
    {
        private CompanyService CompanyServiceClient { get; }
        private IRestRequest Request { get; set; }
        private Mock<IServiceClient> MockServiceClient { get; }


        public CompanyDbClientTests()
        {
            MockServiceClient = new Mock<IServiceClient>();
            CompanyServiceClient = new CompanyService(MockServiceClient.Object);
        }

        [Fact]
        public async void Client_SearchByNameOrCin()
        {
            List<CompanySearchResponse> test = GetCompanySearchResponseList();
            var response = new CompanySearchResponses
            {
                CompanySearchResponse = test,
                ReferenceNumber = "referencenumber"
            };
            MockServiceClient.Setup(s => s.ExecuteAsync<CompanySearchResponses>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(response)
                .Callback<IRestRequest>(r => Request = r);

            var result = await CompanyServiceClient.SearchByNameOrCin("app","111","U67120MH1991PTC063321");
            Assert.Equal("{entityType}/{entityId}/company/search/{s}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(response);
        }

        [Fact]
        public async void Client_GetCompanyWithDirectors()
        {
            CompanyWithDirectors test = GetCompanyWithDirectorsList();
            MockServiceClient.Setup(s => s.ExecuteAsync<CompanyWithDirectors>(It.IsAny<IRestRequest>()))
              .ReturnsAsync(test)
               .Callback<IRestRequest>(r => Request = r);

            var result = await CompanyServiceClient.GetCompanyWithDirectors("app","111","U67120MH1991PTC063321");
            Assert.Equal("{entityType}/{entityId}/company/directors/{cin}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_GetCompanyCategory()
        {
            RuleResult test = GetRuleResult();
            ICompanyWithDirectors companyWithDirectors = iCompanyWithDirectors();
            MockServiceClient.Setup(s => s.ExecuteAsync<RuleResult>(It.IsAny<IRestRequest>()))
             .ReturnsAsync(test)
              .Callback<IRestRequest>(r => Request = r);

            var result = await CompanyServiceClient.GetCompanyCategory("app","111",companyWithDirectors);
            Assert.Equal("{entityType}/{entityId}/company/category/{cin}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        private RuleResult GetRuleResult()
        {
            return new RuleResult()
            {
                Result= "D",
                RuleName = "LLP or Partnership"
            };
        }
        private CompanyWithDirectors GetCompanyWithDirectorsList()
        {            
            CompanyWithDirectors companyWithDirector = new CompanyWithDirectors();
            List<CompanyDirector> companyDirector = new List<CompanyDirector>();
            companyDirector.Add(new CompanyDirector()
            {
                Cin = "U67120MH1991PTC063321",
                Din = "'01551588",
                Name = "SHEGGY GUPTA",
                AgeOfCompany = 8,
                PaidUpCapital = 100000
            });
            var Directors = companyDirector;

            var Company = new Company()
            {
                AgeOfCompany = 8,
                AuthorisedCapital = 100000,
                BankCategory = "",
                Domain = "www.infosys.com",
                ExperianCategory = "Cat A",
                RblDefaulters = "TRUE",
                Category = "Non-govt company",
                Cin = "U67120MH1991PTC063321",
                Name = "B & A CREATIONS PRIVATE LIMITED",
                LastYearProfit = 0,
                LastYearRevenue = 0,
                MoneyControlCompanyName = "",
                MoneyControlProfit = 0,
                MoneyControlRevenue = 0,
                MoneyControlUrl = "",
                NavratnaCategory = "NA",
                PaidUpCapital = 100000,
                StatusOfListing = "Unlisted",
                SubCategory = "Others",
                Type = "Private",
                ZaubaCorpUrl = "https://www.zaubacorp.com/company/B-A-CREATIONS-PRIVATE-LIMITED/U18109DL2008PTC177167"
            };
            companyWithDirector.Directors = Directors;
            companyWithDirector.Company = Company;            
            return companyWithDirector;
        }
        public ICompanyWithDirectors iCompanyWithDirectors()
        {
            ICompanyWithDirectors companyWithDirector = new CompanyWithDirectors();
            List<CompanyDirector> companyDirector = new List<CompanyDirector>();
            companyDirector.Add(new CompanyDirector()
            {
                Cin = "U67120MH1991PTC063321",
                Din = "'01551588",
                Name = "SHEGGY GUPTA",
                AgeOfCompany = 8,
                PaidUpCapital = 100000
            });
            var Directors = companyDirector;

            var Company = new Company()
            {
                AgeOfCompany = 8,
                AuthorisedCapital = 100000,
                BankCategory = "",
                Domain = "www.infosys.com",
                ExperianCategory = "Cat A",
                RblDefaulters = "TRUE",
                Category = "Non-govt company",
                Cin = "U67120MH1991PTC063321",
                Name = "B & A CREATIONS PRIVATE LIMITED",
                LastYearProfit = 0,
                LastYearRevenue = 0,
                MoneyControlCompanyName = "",
                MoneyControlProfit = 0,
                MoneyControlRevenue = 0,
                MoneyControlUrl = "",
                NavratnaCategory = "NA",
                PaidUpCapital = 100000,
                StatusOfListing = "Unlisted",
                SubCategory = "Others",
                Type = "Private",
                ZaubaCorpUrl = "https://www.zaubacorp.com/company/B-A-CREATIONS-PRIVATE-LIMITED/U18109DL2008PTC177167"
            };
            companyWithDirector.Directors = Directors;
            companyWithDirector.Company = Company;
            return companyWithDirector;
        }       
        private List<CompanySearchResponse> GetCompanySearchResponseList()
        {
            CompanySearchResponse companysearchrespons = new CompanySearchResponse()
            {
                Cin = "U67120MH1991PTC063321",
                CompanyCategory = "Non-govt company",
                BankCategory = "",
                NavratnaCategory = "NA",
                Name = "B & A CREATIONS"
            };
            List<CompanySearchResponse> companysearchresponses = new List<CompanySearchResponse>();
            companysearchresponses.Add(companysearchrespons);
            return companysearchresponses;
        }
    }
}
