﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Moq;
using CreditExchange.CompanyDb.Abstractions;
using CreditExchange.CompanyDb.Mocks;
using Microsoft.AspNet.Mvc;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Logging;
using CreditExchange.CompanyDb.Api;
using LendFoundry.EventHub.Client;
using CreditExchange.Foundation.Validator;

namespace CreditExchange.CompanyDb.Api.Tests
{
    public class CompanyDbControllerTests
    {
        public CompanyDbControllerTests()
        {
        }

        [Fact]
        public void Given_A_Correct_CIN_SearchAsync_Return_Ok()
        {           
            // ARRANGE

            var logger = new Mock<ILogger>();
            var mockRepo = new Mock<ICompanyDbRepository>();
            var mockCompaniesList = GetCompanyList();           
            mockRepo.Setup(x => x.SearchByNameOrCin("ABC")).Returns(Task.FromResult(mockCompaniesList.AsQueryable()));
            var sut = new CompanyService(mockRepo.Object, Mock.Of<IEventHubClient>(), Mock.Of<IEntityValidator>());
            var controller = new CompanyDbController(sut, logger.Object);                      

            // ACT

            var searchResults = controller.SearchAsync("app","1","ABC").Result;

            //Assert
            Assert.NotNull(((ObjectResult)searchResults).Value);
            Assert.IsType<HttpOkObjectResult>(searchResults);
            Assert.Equal(((ObjectResult)searchResults).StatusCode, 200);
        }

        [Fact]
        public void Given_A_Wrong_CIN_SearchAsync_Return_Count_Zero()
        {
            // ARRANGE

            var logger = new Mock<ILogger>();
            var mockRepo = new Mock<ICompanyDbRepository>();
            var mockCompaniesList = GetCompanyList();
            mockRepo.Setup(x => x.SearchByNameOrCin("ABC")).Returns(Task.FromResult(mockCompaniesList.AsQueryable()));
            var sut = new CompanyService(mockRepo.Object, Mock.Of<IEventHubClient>(), Mock.Of<IEntityValidator>());
            var controller = new CompanyDbController(sut, logger.Object);

            // ACT

            var searchResults = controller.SearchAsync("application", "111", "xyz").Result;

            //Assert
            Assert.IsType<HttpOkObjectResult>(searchResults);
            Assert.Equal(((ObjectResult)searchResults).StatusCode, 200);
            Assert.Equal(((CompanySearchResponses)((ObjectResult)searchResults).Value).CompanySearchResponse.Count, 0);            
        }

        [Fact]
        public void Given_A_Null_CIN_SearchAsync_Throws_Exception()
        {
            // ARRANGE

            var logger = new Mock<ILogger>();
            var mockRepo = new Mock<ICompanyDbRepository>();
            var mockCompaniesList = GetCompanyList();
            mockRepo.Setup(x => x.SearchByNameOrCin("ABC")).Returns(Task.FromResult(mockCompaniesList.AsQueryable()));
            var sut = new CompanyService(mockRepo.Object, Mock.Of<IEventHubClient>(), Mock.Of<IEntityValidator>());
            var controller = new CompanyDbController(sut, logger.Object);

            //Act & Assert

            Assert.ThrowsAsync<ArgumentNullException>(() => controller.SearchAsync("application", "111", null));
        }

        [Fact]
        public void Given_A_Correct_Cin_GetCompanyCategory_Returns_Ok()
        {
            // ARRANGE
            var logger = new Mock<ILogger>();
            var mockRepo = new Mock<ICompanyDbRepository>();
            var sut = new Mock<ICompanyService>();
            var mockcompanyWithDirectors = GetCompanyWithDirectorsList();
            var ruleResult = new RuleResult();
            sut.Setup(x => x.GetCompanyCategory("application", "111", mockcompanyWithDirectors)).ReturnsAsync(ruleResult);
            sut.Setup(x => x.GetCompanyWithDirectors("application", "111", "U67120MH1991PTC063321")).Returns(Task.FromResult(mockcompanyWithDirectors));
            var controller = new CompanyDbController(sut.Object, logger.Object);

            // ACT
            var searchResults = controller.GetCompanyCategory("U67120MH1991PTC063321", "application","asd").Result;

            //Assert
            Assert.IsType<HttpOkObjectResult>(searchResults);
            Assert.Equal(((ObjectResult)searchResults).StatusCode, 200);
        }

        [Fact]
        public void Given_A_Wrong_Cin_GetCompanyCategory_Throws_Exception()
        {
            // ARRANGE
            var logger = new Mock<ILogger>();
            var mockRepo = new Mock<ICompanyDbRepository>();
            var sut = new Mock<ICompanyService>();
            var mockcompanyWithDirectors = GetCompanyWithDirectorsList();
            var ruleResult = new RuleResult();
            sut.Setup(x => x.GetCompanyCategory("application", "111", mockcompanyWithDirectors)).ReturnsAsync(ruleResult);
            sut.Setup(x => x.GetCompanyWithDirectors("application", "111", "U67120MH1991PTC063321")).Returns(Task.FromResult(mockcompanyWithDirectors));
            var controller = new CompanyDbController(sut.Object, logger.Object);

            // ACT
           // var searchResults = controller.GetCompanyCategory("XXX", "application", "asd").Result;

            //Assert
            Assert.ThrowsAsync<AggregateException>(() => controller.GetCompanyCategory("XXX", "application", "asd"));
        }
        [Fact]        
        public void Given_A_Null_Cin_GetCompanyCategory_Throws_Exception()
        {
            //Assert
            var logger = new Mock<ILogger>();
            var mockRepo = new Mock<ICompanyDbRepository>();
            var mockCompaniesList = GetCompanyList();
            mockRepo.Setup(x => x.SearchByNameOrCin("ABC")).Returns(Task.FromResult(mockCompaniesList.AsQueryable()));
            var sut = new CompanyService(mockRepo.Object, Mock.Of<IEventHubClient>(), Mock.Of<IEntityValidator>());
            var controller = new CompanyDbController(sut, logger.Object);

            //Act & Assert
            Assert.ThrowsAsync<ArgumentNullException>(() => controller.GetCompanyCategory(null, null, null));
        }

        [Fact]
        public void Given_A_Valid_Cin_GetCompanyWithDirectors_Returns_Ok()
        {
            //Arrange
            var logger = new Mock<ILogger>();
            var mockRepo = new Mock<ICompanyDbRepository>();
            var mockcompanyWithDirectors = GetCompanyWithDirectorsList();
            mockRepo.Setup(x => x.GetCompany("U67120MH1991PTC063321")).Returns(Task.FromResult(mockcompanyWithDirectors));
            var sut = new CompanyService(mockRepo.Object, Mock.Of<IEventHubClient>(), Mock.Of<IEntityValidator>());
            var controller = new CompanyDbController(sut, logger.Object);

            //Act
            var response = controller.GetCompanyWithDirectors("app","111","U67120MH1991PTC063321");

            //Assert
            Assert.IsType<HttpOkObjectResult>(response.Result);
            Assert.Equal(((ObjectResult)response.Result).StatusCode, 200);
            Assert.NotEqual(((CompanyWithDirectors)(((ObjectResult)response.Result)).Value).Directors.Count, 0);
        }

        [Fact]
        public void Given_A_Valid_Cin_GetCompanyWithDirectors_Returns_Null()
        {
            //Arrange
            var logger = new Mock<ILogger>();
            var mockRepo = new Mock<ICompanyDbRepository>();
            var mockCompaniesList = GetCompanyList();
            var mockcompanyWithDirectors = GetCompanyWithDirectorsList();
            mockRepo.Setup(x => x.GetCompany("U67120MH1991PTC063321")).ReturnsAsync(mockcompanyWithDirectors);
            var sut = new CompanyService(mockRepo.Object, Mock.Of<IEventHubClient>(), Mock.Of<IEntityValidator>());

            var controller = new CompanyDbController(sut, logger.Object);

            //Act
            var response = controller.GetCompanyWithDirectors("app","111", "U67120MH1991PTC063321");

            //Assert
            Assert.IsType<HttpOkObjectResult>(response.Result);
            Assert.Equal(((ObjectResult)response.Result).StatusCode, 200);
            Assert.NotNull((CompanyWithDirectors)(((ObjectResult)response.Result)).Value);
        }
        [Fact]
        public void Given_Null_Value_For_Cin_GetCompanyWithDirectors_Throws_Exception()
        {
            //Arrange
            var logger = new Mock<ILogger>();
            var mockRepo = new Mock<ICompanyDbRepository>();
            var mockcompanyWithDirectors = GetCompanyWithDirectorsList();
            mockRepo.Setup(x => x.GetCompany("U67120MH1991PTC063321")).Returns(Task.FromResult(mockcompanyWithDirectors));
            var sut = new CompanyService(mockRepo.Object, Mock.Of<IEventHubClient>(), Mock.Of<IEntityValidator>());

            var controller = new CompanyDbController(sut, logger.Object);

            //Act

            Assert.ThrowsAsync<ArgumentNullException>(() => controller.GetCompanyWithDirectors("app","111",null));           
        }

        private List<Company> GetCompanyList()
        {
            List<Company> listOfCompany = new List<Company>();
            listOfCompany.Add(new Company()
            {
                AgeOfCompany = 8,
                AuthorisedCapital = 100000,
                BankCategory = "",
                Domain = "www.infosys.com",
                ExperianCategory = "Cat A",
                RblDefaulters = "TRUE",
                Category = "Non-govt company",
                Cin = "U67120MH1991PTC063321",
                Name = "B & A CREATIONS PRIVATE LIMITED",
                LastYearProfit = 0,
                LastYearRevenue = 0,
                MoneyControlCompanyName = "",
                MoneyControlProfit = 0,
                MoneyControlRevenue = 0,
                MoneyControlUrl = "",
                NavratnaCategory = "NA",
                PaidUpCapital = 100000,
                StatusOfListing = "Unlisted",
                SubCategory = "Others",
                Type = "Private",
                ZaubaCorpUrl = "https://www.zaubacorp.com/company/B-A-CREATIONS-PRIVATE-LIMITED/U18109DL2008PTC177167"
            });

            listOfCompany.Add(new Company()
            {
                AgeOfCompany = 81,
                AuthorisedCapital = 100000000,
                BankCategory = "",
                Domain = "www.wipro.com",
                ExperianCategory = "Cat A",
                RblDefaulters = "TRUE",
                Category = "Non-govt company",
                Cin = "U18109DL2008PTC7",
                Name = "B & A Limited",
                LastYearProfit = 0,
                LastYearRevenue = 0,
                MoneyControlCompanyName = "B and A",
                MoneyControlProfit = 0,
                MoneyControlRevenue = 0,
                MoneyControlUrl = "",
                NavratnaCategory = "NA",
                PaidUpCapital = 1000090,
                StatusOfListing = "Unlisted",
                SubCategory = "Others",
                Type = "Private",
                ZaubaCorpUrl = "https://www.zaubacorp.com/company/B-A-CREATIONS-PRIVATE-LIMITED/U18109DL2008PTC177167"
            });

            return listOfCompany;
        }

        private CompanyWithDirectors GetCompanyWithDirectorsList()
        {
            CompanyWithDirectors companyWithDirector = new CompanyWithDirectors();
            List<CompanyDirector> companyDirector = new List<CompanyDirector>();
            companyDirector.Add(new CompanyDirector()
            {
                Cin = "U67120MH1991PTC063321",
                Din = "'01551588",
                Name = "SHEGGY GUPTA",
                AgeOfCompany = 8,
                PaidUpCapital = 100000
            });
            var Directors = companyDirector;

            var Company = new Company()
            {
                AgeOfCompany = 8,
                AuthorisedCapital = 100000,
                BankCategory = "",
                Domain = "www.infosys.com",
                ExperianCategory = "Cat A",
                RblDefaulters = "TRUE",
                Category = "Non-govt company",
                Cin = "U67120MH1991PTC063321",
                Name = "B & A CREATIONS PRIVATE LIMITED",
                LastYearProfit = 0,
                LastYearRevenue = 0,
                MoneyControlCompanyName = "",
                MoneyControlProfit = 0,
                MoneyControlRevenue = 0,
                MoneyControlUrl = "",
                NavratnaCategory = "NA",
                PaidUpCapital = 100000,
                StatusOfListing = "Unlisted",
                SubCategory = "Others",
                Type = "Private",
                ZaubaCorpUrl = "https://www.zaubacorp.com/company/B-A-CREATIONS-PRIVATE-LIMITED/U18109DL2008PTC177167"
            };
            companyWithDirector.Directors = Directors;
            companyWithDirector.Company = Company;
            return companyWithDirector;
        }
    }
}
