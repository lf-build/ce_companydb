﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditExchange.CompanyDb.Abstractions;
using System.Globalization;

namespace CreditExchange.CompanyDb.Mocks
{
    public class FakeCompanyService : ICompanyService
    {
        public List<CompanySearchResponse> CompanySearchResponse { get; } = new List<CompanySearchResponse>();
        public List<Company> Company { get; } = new List<Company>();
        public List<CompanyDirector> CompanyDirector { get; } = new List<CompanyDirector>();
        public FakeCompanyService(IEnumerable<CompanySearchResponse> companySearchResponse)
        {
            CompanySearchResponse.AddRange(companySearchResponse);
        }

        public FakeCompanyService(IEnumerable<Company> company, IEnumerable<CompanyDirector> companyDirector)
        {
            Company.AddRange(company);
            CompanyDirector.AddRange(companyDirector);
        }

        public Task<RuleResult> GetCompanyCategory(string entityType, string entityId, ICompanyWithDirectors companyWithDirectors)
        {
            var rulesParams = GetParametersFromCompany(companyWithDirectors);
            var ruleresult = new RuleResult();
            if (companyWithDirectors.Company.StatusOfListing.ToLower() == "unlisted" && companyWithDirectors.Company.Category.ToLower() == "government company")
            {

                CompanyCategorizationRuleBase unlistedGovtRule = new UnlistedGovtRule(rulesParams, ruleresult, entityType, entityId);

                unlistedGovtRule.ExecuteRule();
                return Task.Run(() => ruleresult);
            }
            if (companyWithDirectors.Company.StatusOfListing.ToLower() == "unlisted" && companyWithDirectors.Company.Category.ToLower() == "non-govt company")
            {
                CompanyCategorizationRuleBase unlistedNonGovtRule = new UnlistedNonGovtRules(rulesParams, ruleresult, entityType, entityId);

                unlistedNonGovtRule.ExecuteRule();
                return Task.Run(() => ruleresult);
            }

            if (companyWithDirectors.Company.StatusOfListing.ToLower() == "listed" && companyWithDirectors.Company.Category.ToLower() == "government company")
            {
                CompanyCategorizationRuleBase listedGovtRule = new ListedGovtRules(rulesParams, ruleresult);

                listedGovtRule.ExecuteRule();
                return Task.Run(() => ruleresult);
            }

            if (companyWithDirectors.Company.StatusOfListing.ToLower() == "listed" && companyWithDirectors.Company.Category.ToLower() == "non-govt company")
            {
                CompanyCategorizationRuleBase listedNonGovtRole = new ListedNonGovtRules(rulesParams, ruleresult);

                listedNonGovtRole.ExecuteRule();
                return Task.Run(() => ruleresult);
            }
            // if none of the conditions then default score is applied.
            ruleresult.Result = "D";
            ruleresult.RuleName = "LLP or Partnership";
            return Task.Run(() => ruleresult);
        }

        public Task<CompanyWithDirectors> GetCompanyWithDirectors(string entityType, string entityId, string cin)
        {
            FakeCompanyRepository fakeCompanyRepository = new FakeCompanyRepository(Company, CompanyDirector);

            var companywithDirectors = fakeCompanyRepository.GetCompany(cin);

            return companywithDirectors;
        }

        public Task<CompanySearchResponses> SearchByNameOrCin(string entityType, string entityId, string searchText)
        {
            var searchResults = Company.AsQueryable().
                   Where(c => c.Name.ToLower().StartsWith(searchText.ToLower())
                              ||
                              c.Cin.ToLower().StartsWith(searchText.ToLower())).Take(50);

            var searchProjection = searchResults.Select(c => new CompanySearchResponse
            {

                Name = c.Name,
                CompanyCategory = c.Category,
                Cin = c.Cin,
                BankCategory = c.BankCategory,
                NavratnaCategory = c.NavratnaCategory

            }).OrderBy(c => c.Name).Take(15).ToList();
            var result = new CompanySearchResponses
            {
                CompanySearchResponse = searchProjection,
                ReferenceNumber = Guid.NewGuid().ToString("N")
            };
            return Task.FromResult(result);
        }

        private static CompanyCategorizationRulesParameters GetParametersFromCompany(ICompanyWithDirectors companyWithDirectors)
        {
            var companycategorizatonrulesParams = new CompanyCategorizationRulesParameters
            {

                BankCategory = companyWithDirectors.Company.BankCategory,
                AgeOfCompany = companyWithDirectors.Company.AgeOfCompany.ToString(),
                AuthorizedCapital = companyWithDirectors.Company.AuthorisedCapital.ToString(CultureInfo.InvariantCulture),
                Navratna = companyWithDirectors.Company.NavratnaCategory != "NA",
                PaidUpCapital = companyWithDirectors.Company.PaidUpCapital.ToString(CultureInfo.InvariantCulture),
                Profit = companyWithDirectors.Company.MoneyControlProfit == 0 ? "0" : companyWithDirectors.Company.MoneyControlProfit.ToString(CultureInfo.InvariantCulture),
                Revenue = companyWithDirectors.Company.MoneyControlRevenue == 0 ? "0" : companyWithDirectors.Company.MoneyControlRevenue.ToString(CultureInfo.InvariantCulture),
                DirectorsList = companyWithDirectors.Directors.ToArray(),
                Cin = companyWithDirectors.Company.Cin,
                IsState = companyWithDirectors.Company.SubCategory.ToLower() == "state"
            };

            return companycategorizatonrulesParams;
        }
    }
}
