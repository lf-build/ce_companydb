﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CreditExchange.CompanyDb.Abstractions;

namespace CreditExchange.CompanyDb.Mocks
{
    public class FakeCompanyRepository : ICompanyDbRepository
    {
        public List<CompanySearchResponse> CompanySearchResponse { get; } = new List<CompanySearchResponse>();
        public List<Company> Company { get; } = new List<Company>();
        public List<CompanyDirector> CompanyDirector { get; } = new List<CompanyDirector>();
        public FakeCompanyRepository(IEnumerable<CompanySearchResponse> companySearchResponse)
        {
            CompanySearchResponse.AddRange(companySearchResponse);
        }

        public FakeCompanyRepository(IEnumerable<Company> company, IEnumerable<CompanyDirector> companyDirector)
        {
            Company.AddRange(company);
            CompanyDirector.AddRange(companyDirector);
        }
        public Task<CompanyWithDirectors> GetCompany(string cin)
        {
            var company = Company.Where(c => c.Cin == cin);

            var directors = CompanyDirector.Where(d => d.Cin == cin);

            var companywithDirectors = new CompanyWithDirectors
            {
                Company = company.FirstOrDefault(),
                Directors = directors.ToList()
            };
            return Task.FromResult(companywithDirectors);
        }

        public Task<bool> InsertDirectors(IEnumerable<CompanyDirector> companyDirectors)
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<Company>> SearchByNameOrCin(string searchText)
        {
            //var searchResults = Company.Where(c => c.Name.ToLower().StartsWith(searchText.ToLower())
            //                        ||
            //                  c.Cin.ToLower().StartsWith(searchText.ToLower())).Take(50);
            //return Task.FromResult(searchResults.AsQueryable());


            return Task.Run(() =>
            {
                var searchResults = Company.AsQueryable().
                    Where(c => c.Name.ToLower().StartsWith(searchText.ToLower())
                               ||
                               c.Cin.ToLower().StartsWith(searchText.ToLower())).Take(50);
                return searchResults;
            });
        }

        public Task<bool> UpsertCompany(IEnumerable<Company> companyMasterList)
        {
            throw new NotImplementedException();
        }
    }
}
