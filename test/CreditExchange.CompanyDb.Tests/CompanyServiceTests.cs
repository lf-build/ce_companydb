﻿using CreditExchange.CompanyDb;
using CreditExchange.CompanyDb.Abstractions;
using CreditExchange.Foundation.Validator;
using LendFoundry.EventHub.Client;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.Company.Tests
{
    public class CompanyServiceTests
    {
        [Fact]
        public void Given_Proper_Cin_Then_Search_Should_Return_Company()
        {
            // ARRANGE
            var mockRepo = new Mock<ICompanyDbRepository>();
            var mockCompaniesList = new List<CompanyDb.Abstractions.Company>
            {
                new CompanyDb.Abstractions.Company
            {
                Cin = "AAAA111",
                Category = "A",
                AgeOfCompany = 1,
                AuthorisedCapital = 10000,
                Domain = "mycompany.com"
            },
            new CompanyDb.Abstractions.Company
            {
                Cin = "BBBB2222",
                Category = "B",
                AgeOfCompany = 10,
                AuthorisedCapital = 30000,
                Domain = "mycompany1.com"
            }
        };



            mockRepo.Setup(x => x.SearchByNameOrCin("ABC")).Returns(Task.FromResult(mockCompaniesList.AsQueryable()));
            var sut = new CompanyService(mockRepo.Object,Mock.Of<IEventHubClient>(), Mock.Of<IEntityValidator>());

            // ACT

            var searchResults = sut.SearchByNameOrCin("app","111","ABC").Result;

            //ASSERT

            Assert.Equal(mockCompaniesList.Count(), searchResults.CompanySearchResponse.Count);


        }

        [Fact]
        [Trait("CompanyCategory", "UnlistedGovtRule")]
        public void Given_That_Unlisted_Govt_Company_Is_Navratna_Then_Category_Should_Be_A()
        {
            // ARRANGE

            var categorizatonParams = new CompanyCategorizationRulesParameters
            {
                AgeOfCompany = "10",
                AuthorizedCapital = "1500000",
                Navratna = true,
                Cin = "XXXX"
            };
            var ruleResult = new RuleResult();
            var sut = new UnlistedGovtRule(categorizatonParams, ruleResult,string.Empty,string.Empty);

            // ACT
            sut.ExecuteRule();



            // ASSERT

            Assert.Equal("A", ruleResult.Result);
            Assert.Equal(nameof(UnlistedGovtRule), ruleResult.RuleName);
        }

        [Fact]
        [Trait("CompanyCategory", "UnlistedNonGovtRules")]
        public void Given_Unlisted_NonGovt_Company_Bank_Category_Exists_Then_It_Should_Return_The_Corresponding_BankCategory()
        {
            // ARRANGE

            var categorizatonParams = new CompanyCategorizationRulesParameters
            {
                AgeOfCompany = "10",
                AuthorizedCapital = "1500000",
                BankCategory = "A",
                Cin = "XXXX"
            };
            var ruleResult = new RuleResult();
            var sut = new UnlistedNonGovtRules(categorizatonParams, ruleResult,string.Empty,string.Empty);

            // ACT
            sut.ExecuteRule();



            // ASSERT

            Assert.Equal(categorizatonParams.BankCategory, ruleResult.Result);
            Assert.Equal(nameof(UnlistedNonGovtRules), ruleResult.RuleName);
        }

        [Fact]
        [Trait("CompanyCategory", "ListedNonGovtRules")]
        public void Given_That_Listed_NonGovt_Company_Is_Navratna_Then_Category_Should_Be_A()
        {
            // ARRANGE

            var categorizatonParams = new CompanyCategorizationRulesParameters
            {
                AgeOfCompany = "10",
                AuthorizedCapital = "1500000",
                Navratna = true,
                BankCategory = "A",
                Cin = "XXXX"
            };
            var ruleResult = new RuleResult();
            var sut = new ListedNonGovtRules(categorizatonParams, ruleResult);

            // ACT
            sut.ExecuteRule();



            // ASSERT

            Assert.Equal(categorizatonParams.BankCategory, ruleResult.Result);
            Assert.Equal(nameof(ListedNonGovtRules), ruleResult.RuleName);
        }


        [Fact]
        [Trait("CompanyCategory", "UnlistedGovtBankCategoryRule")]
        public void Given_That_Unlisted_Govt_Company_Has_BankCategory_Then_It_Should_Return_That_Corresponding_Bank_Category()
        {

        }

    }
}
